<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_filter( 'body_class', 'understrap_body_classes' );
if ( ! function_exists( 'understrap_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 *
	 * @return array
	 */
	function understrap_body_classes( $classes ) {
		// Adds a class of group-blog to blogs with more than 1 published author.
		if ( is_multi_author() ) {
			$classes[] = 'group-blog';
		}

		// Adds a class of hfeed to non-singular pages.
		if ( !is_singular() ) {
			$classes[] = 'hfeed';
		}

		foreach ( $classes as $key => $value ) {
			if ( 'tag' == $value ) {
				unset( $classes[ $key ] );
			}
		}

		return $classes;
	}
}

add_filter( 'admin_body_class', 'understrap_admin_body_classes' );
if ( ! function_exists( 'understrap_admin_body_classes' ) ) {
	/**
	 * Adds custom classes to the array of admin body classes.
	 *
	 * @param string $classes Classes for the body element.
	 *
	 * @return string
	 */
	function understrap_admin_body_classes( $classes ) {

		$current_screen = get_current_screen();

		// If we are on the Edit Post page (for any custom post type), see if this user can delete pages, and inject
		// the corresponding class accordingly, which allows us to hide certain features on the page
		if ( $current_screen->base == 'post' ) {
			$post_type_obj = get_post_type_object( $current_screen->post_type );

			if ( is_object( $post_type_obj->cap ) && !empty( $post_type_obj->cap->delete_posts ) && !current_user_can( $post_type_obj->cap->delete_posts ) ) {
				$classes .= ' user-cannot-delete';
			}
		}

		return $classes;
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */

if ( ! function_exists ( 'understrap_post_nav' ) ) {
	function understrap_post_nav() {
		// Don't print empty markup if there's nowhere to navigate.
		$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
		$next     = get_adjacent_post( false, '', false );

		if ( ! $next && ! $previous ) {
			return;
		}

		$container = get_theme_mod( 'understrap_container_type', 'container' );
		?>
		<nav class="<?php echo esc_attr( $container ); ?> navigation post-navigation">
			<h2 class="sr-only"><?php esc_html_e( 'Post navigation', 'understrap' ); ?></h2>
			<div class="row nav-links justify-content-between">
				<?php
				if ( get_previous_post_link() ) {
					previous_post_link( '<span class="nav-previous">%link</span>', _x( '<i class="fa fa-angle-left"></i>&nbsp;%title', 'Previous post link', 'understrap' ) );
				}
				if ( get_next_post_link() ) {
					next_post_link( '<span class="nav-next">%link</span>', _x( '%title&nbsp;<i class="fa fa-angle-right"></i>', 'Next post link', 'understrap' ) );
				}
				?>
			</div><!-- .nav-links -->
		</nav><!-- .navigation -->
		<?php
	}
}

/**
 * Display navigation to next/previous post when applicable.
 */

add_action( 'template_redirect', 'gradpress_hide_authors', 1 );
if ( ! function_exists ( 'gradpress_hide_authors' ) ) {
	function gradpress_hide_authors() {
		if ( is_author() ) {
			$GLOBALS[ 'wp_query' ]->set_404();
			status_header( 404 );
			wp_redirect( '/not-found' );
		}
	}
}