<?php
/**
 * Theme basic setup.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

add_action( 'after_setup_theme', 'understrap_setup' );
if ( ! function_exists ( 'understrap_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function understrap_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() above and below the main page header.
		register_nav_menus( array(
			'primary' => 'Primary Navigation',
			'modal' => 'Modal Navigation'
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Adding Thumbnail basic support
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Adding support for Widget edit icons in customizer
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'understrap_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Set up the WordPress Theme logo feature.
		add_theme_support( 'custom-logo' );
	}
}

add_filter( 'excerpt_more', 'understrap_custom_excerpt_more' );
if ( ! function_exists( 'understrap_custom_excerpt_more' ) ) {
	/**
	 * Removes the ... from the excerpt read more link
	 *
	 * @param string $more The excerpt.
	 *
	 * @return string
	 */
	function understrap_custom_excerpt_more( $more ) {
		return '';
	}
}

add_filter( 'wp_trim_excerpt', 'understrap_all_excerpts_get_more_link' );
if ( ! function_exists( 'understrap_all_excerpts_get_more_link' ) ) {
	/**
	 * Adds a custom read more link to all excerpts, manually or automatically generated
	 *
	 * @param string $post_excerpt Posts's excerpt.
	 *
	 * @return string
	 */
	function understrap_all_excerpts_get_more_link( $post_excerpt ) {
		return $post_excerpt . ' <a class="understrap-read-more-link" href="' . esc_url( get_permalink( get_the_ID() )) . '">[&hellip;]</a>';
	}
}

/*
 * Capture wp_head output to remove unnecessary meta tags later (notice the priority for this hook)
 */
add_action( 'wp_head', 'gradpress_wp_head_ob_start', 1 );
if ( !function_exists( 'gradpress_wp_head_ob_start' ) ) {
	function gradpress_wp_head_ob_start() {
		ob_start();
	}
}

// Remove unnecessary wp_head hooks
add_action( 'after_setup_theme', 'gradpress_remove_wp_head_hooks' );
if ( !function_exists( 'gradpress_remove_wp_head_hooks' ) ) {
	function gradpress_remove_wp_head_hooks () {

		// Remove Windows Live Writer Manifest and links
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wp_shortlink_wp_head' );

		// Remove styles for supporting emojis
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );

		// Remove the REST API lines from the HTML Header
		remove_action( 'wp_head', 'rest_output_link_wp_head' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

		// Turn off oEmbed auto discovery.
		add_filter( 'embed_oembed_discover', '__return_false' );
	}
}

/**
 * Defines a few CSS rules to move the admin bar to the bottom of the page (so that it doesn't interfere with sticky headers and navigations), and address other layout issues
 */
add_action( 'wp_head', 'understrap_wp_head', 100 );
if ( ! function_exists( 'understrap_wp_head' ) ) {
	function understrap_wp_head() {
		// Read the Customizer settings and output them as inline CSS
		echo '<style type="text/css">';
		if ( !empty( get_theme_mod( 'color_dark' ) ) ) {
			echo 'body{color:' . get_theme_mod( 'color_dark' ) . '}';
		}
		$bootstrap_colors = array( 'primary', 'secondary', 'light', 'success', 'danger', 'warning', 'info', 'muted' );
		foreach ( $bootstrap_colors as $a_color ) {
			$hex_color = get_theme_mod( 'color_' . $a_color );
			if ( !empty( $hex_color ) ) {
				echo '*[class*="text-' . $a_color . '"],*[class*="text-' . $a_color . '"] *{color:' . $hex_color . '!important}';
				echo '*[class*="bg-' . $a_color . '"],*[class*="btn-' . $a_color . '"]{background-color:' . $hex_color . '!important}';
				echo '*[class*="border-' . $a_color . '"]{border-color:' . $hex_color . '!important}';
			}
		}

		// The primary color is also associated to links
		$color_primary = get_theme_mod( 'color_primary' );
		if ( !empty( $color_primary ) ) {
			echo 'a{color:' . $color_primary . '}';
		}

		// Primary and secondary colors can be assigned a 'hover' state
		$color_primary_hover = get_theme_mod( 'color_primary_hover' );
		$color_secondary_hover = get_theme_mod( 'color_secondary_hover' );
		if ( !empty( $color_primary_hover ) ) {
			echo 'a:hover,*[class*="text-primary"]:hover{color:' . $color_primary_hover . '}';
			echo '*[class*="text-secondary"]:hover{color:' . $color_secondary_hover . '!important}';
		}
		echo "</style>\n";

		// Does this page have any custom HEAD markup code?
		if ( is_page() ) {
			if ( !empty( $GLOBALS[ 'page_settings' ][ 'head_markup' ] ) ) {
				echo wp_unslash( $GLOBALS[ 'page_settings' ][ 'head_markup' ] );
			}
		}

		// Turns on output_buffering
		ini_set( 'output_buffering', 'on' ); 

		// Output buffer was captured, so now we can manipulate it
		$html = ob_get_contents();

		// Remove Meta Generator tags
		$html = preg_replace( '/<meta name="generator"(.*?)>/i', '', $html );
		ob_end_clean();

		echo $html;
	}
}

if ( !empty( $_GET[ 'vc_editable' ] ) && $_GET[ 'vc_editable' ] == 'true' ) {
	add_action( 'get_header', 'gradpress_remove_admin_bar_bump' );
}
function gradpress_remove_admin_bar_bump() {
	remove_action( 'wp_head', '_admin_bar_bump_cb' );
}

/**
 * Changes the appearance of the Customizer screen (larger pane)
 */
add_action( 'customize_controls_print_styles', 'gradpress_customizer_size' );
if ( ! function_exists( 'gradpress_customizer_size' ) ) {
	function gradpress_customizer_size() {
		echo '<style type="text/css">.wp-full-overlay-sidebar{width:25%!important}.wp-full-overlay.collapsed .wp-full-overlay-sidebar{margin-left:-25%}.wp-full-overlay.expanded{margin-left:25%}</style>'."\n";
	}
}

/**
 * Outputs custom markup defined in the theme settings, right before the closing BODY tag
 */
add_action( 'wp_footer', 'understrap_wp_footer' );
if ( ! function_exists( 'understrap_wp_footer' ) ) {
	function understrap_wp_footer() {
		$html_code = get_theme_mod( 'understrap_additional_html_code' );
		if ( !empty( $html_code ) ) {
			echo $html_code;
		}

		if ( is_page() ) {
			if ( !empty( $GLOBALS[ 'page_settings' ][ 'footer_markup' ] ) ) {
				echo wp_unslash( $GLOBALS[ 'page_settings' ][ 'footer_markup' ] );
			}
		}
	}
}

/**
 * Removes the 'Category:' prefix from the Category title on the archive page
 */
add_filter( 'get_the_archive_title', 'understrap_category_title' );
if ( !function_exists( 'understrap_category_title' ) ) {
	function understrap_category_title( $_title = '' ) {
		$title_parts = explode( ': ', $_title, 2 );

		if ( !empty( $title_parts[ 1 ] ) ) {
			$_title = wp_kses(
				$title_parts[ 1 ],
				array(
					'span' => array(
						'class' => array(),
					)
				)
			);
		}

		return $_title;
	}
}

/**
 * Get this page's specific configuration
 */
add_action( 'wp', 'gradpress_setup_global_page_settings' );
add_action( 'admin_head', 'gradpress_setup_global_page_settings' );
if ( !function_exists( 'gradpress_setup_global_page_settings' ) ) {
	function gradpress_setup_global_page_settings() {
		$GLOBALS[ 'page_settings' ] = array();

		if ( !empty( $GLOBALS[ 'post' ]->ID ) && is_page() ) {
			$GLOBALS[ 'page_settings' ] = get_post_meta( $GLOBALS[ 'post' ]->ID, '_gradpress_page_settings', true );
		}
		// Events landing page
		else if ( !is_admin() && function_exists( 'tribe_is_past' ) && function_exists( 'tribe_is_upcoming' ) && ( tribe_is_past() || tribe_is_upcoming() ) && !is_tax() ) {
			$page_id = get_theme_mod( 'understrap_breadcrumbs_landing_tribe_events' );
			if ( !empty( $page_id ) ) {
				$GLOBALS[ 'page_settings' ] = get_post_meta( $page_id, '_gradpress_page_settings', true );
			}
		}
	}
}