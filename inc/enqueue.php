<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Load theme's JavaScript and CSS sources.
 */
add_action( 'wp_enqueue_scripts', 'understrap_scripts', 100 );
if ( ! function_exists( 'understrap_scripts' ) ) {
	function understrap_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );
		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
		wp_enqueue_style( 'understrap-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $css_version );

		wp_enqueue_script( 'jquery' );
		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/theme.min.js' );
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/js/theme.min.js', array(), $js_version, true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		understrap_dequeue_wpbakery();

		// Load our customized font icon package for the frontend editor
		if ( !empty( $_REQUEST[ 'vc_editable' ] ) && $_REQUEST[ 'vc_editable' ] == 'true' ) {
			wp_enqueue_style( 'gradpress_frontend_editor', get_stylesheet_directory_uri() . '/css/frontend_editor.min.css', array(), $css_version );
		}
	}
}

/**
 * Prevent WPBakery Page Builder from loading its own styles
 */
add_action( 'vc_base_register_front_css', 'understrap_dequeue_wpbakery', 100 );
if ( ! function_exists( 'understrap_dequeue_wpbakery' ) ) {
	function understrap_dequeue_wpbakery() {
		// The WPBakery CSS/JS files are not needed, as we rely on Bootstrap
		wp_dequeue_style( 'js_composer_front' );
		wp_deregister_style( 'js_composer_front' );

		// We use our own modified version of their script
		wp_dequeue_script( 'wpb_composer_front_js' );
		wp_deregister_script( 'wpb_composer_front_js' );
	}
}


add_action( 'admin_enqueue_scripts', 'understrap_admin_scripts' );
if ( !function_exists( 'understrap_admin_scripts' ) ) {
	function understrap_admin_scripts( $hook ) {
		wp_register_style( 'gradpress_admin_style', get_template_directory_uri() . '/css/admin.css', array(), null );
		wp_enqueue_style( 'gradpress_admin_style' );

		$current_screen = get_current_screen();
		if ( $current_screen->id == 'page' || $current_screen->id == 'customize' ) {
			wp_enqueue_code_editor( array( 'type' => 'text/html' ) );
			wp_enqueue_script( 'understrap-admin-scripts', get_template_directory_uri() . '/js/admin.min.js', array(), null, true );
		}
	}
}