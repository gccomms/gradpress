<?php
/**
 * Compatibility with The Events Calendar
 *
 * @package understrap
 */

/**
 * Filter to change the HTML code used to display event notices (past events, etc)
 */
add_filter( 'tribe_the_notices', 'gradpress_tec_notices', 10, 2 );
if ( !function_exists( 'gradpress_tec_notices' ) ) {
	function gradpress_tec_notices( $_html, $_notices ) {
		$_html = '';

		if ( !empty( $_notices ) ) {
			$_html = '<p class="bg-success text-white p-2">' . implode( ', ', $_notices ) . '</p>';
		}

		return $_html;
	}
}

add_filter( 'tribe_get_events_title', 'gradpress_remove_events_for' );
if ( !function_exists( 'gradpress_remove_events_for' ) ) {
	function gradpress_remove_events_for( $_title = '', $_depth = true ) {
		return str_replace( 
			array(
				'Events for ',
				'Upcoming ',
			),
			array(
				'',
				''
			),
			$_title
		);
	}
}
