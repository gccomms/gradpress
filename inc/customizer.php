<?php
/**
 * Understrap Theme Customizer
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'understrap_theme_customize_register' ) ) {
	/**
	 * Register individual settings through customizer's API.
	 *
	 * @param WP_Customize_Manager $wp_customize Customizer reference.
	 */
	function understrap_theme_customize_register( $wp_customize ) {
		// Layout
		$wp_customize->add_section( 'understrap_layout', array(
			'title' => 'Layout',
			'capability' => 'edit_theme_options',
			'description' => '',
			'priority' => 10
		) );

		// Width
		$wp_customize->add_setting( 'understrap_container_type', array(
			'default' => 'container',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_container_type',
			array(
				'label' => 'Page Width',
				'section' => 'understrap_layout',
				'settings' => 'understrap_container_type',
				'type' => 'select',
				'choices' => array(
					'container' => 'Fixed width',
					'container-fluid' => 'Full width'
				)
			)
		) );

		// Sidebar Position
		$wp_customize->add_setting( 'understrap_sidebar_position', array(
			'default' => 'right',
			'type' => 'theme_mod',
			'sanitize_callback' => 'sanitize_text_field',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_sidebar_position',
			array(
				'label' => 'Sidebar Position on Post Archives',
				'section' => 'understrap_layout',
				'settings' => 'understrap_sidebar_position',
				'type' => 'select',
				'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
				'choices' => array(
					'right' => __( 'Right sidebar', 'understrap' ),
					'left' => __( 'Left sidebar', 'understrap' ),
					'both' => __( 'Left & Right sidebars', 'understrap' ),
					'none' => __( 'No sidebar', 'understrap' )
				)
			)
		) );

		// End Layout
		// --------------------------------------------------------------

		// Colors
		// Adjust priority of Colors section (to keep order)
		if ( !empty( $wp_customize->get_section( 'colors' ) ) ) {
			$wp_customize->get_section( 'colors' )->priority = 20;
		}

		$wp_customize->add_setting( 'color_dark', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control( 
			$wp_customize,
			'color_dark',
			array(
				'label' => 'Body copy',
				'description' => 'Classes: .text-dark, .bg-dark, .border-dark',
				'section' => 'colors',
				'settings' => 'color_dark'
			)
		) );

		$wp_customize->add_setting( 'color_primary', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_primary',
			array(
				'label' => 'Primary',
				'description' => 'Classes: .text-primary, .bg-primary, .border-primary',
				'section' => 'colors',
				'settings' => 'color_primary'
			)
		) );

		$wp_customize->add_setting( 'color_primary_hover', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_primary_hover',
			array(
				'label' => '',
				'description' => 'Classes: .text-primary:hover, .bg-primary:hover, etc',
				'section' => 'colors',
				'settings' => 'color_primary_hover'
			)
		) );

		$wp_customize->add_setting( 'color_secondary', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_secondary',
			array(
				'label' => 'Secondary',
				'description' => 'Classes: .text-secondary, .bg-secondary, .border-secondary',
				'section' => 'colors',
				'settings' => 'color_secondary'
			)
		) );

		$wp_customize->add_setting( 'color_secondary_hover', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_secondary_hover',
			array(
				'label' => '',
				'description' => 'Class: .text-secondary:hover, .bg-secondary:hover, etc',
				'section' => 'colors',
				'settings' => 'color_secondary_hover'
			)
		) );

		$wp_customize->add_setting( 'color_light', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_light',
			array(
				'label' => 'Accent Color 1',
				'description' => 'Classes: .text-light, .bg-light, .border-light',
				'section' => 'colors',
				'settings' => 'color_light'
			)
		) );

		$wp_customize->add_setting( 'color_success', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_success',
			array(
				'label' => 'Accent Color 2',
				'description' => 'Classes: .text-success, .bg-success, .border-success',
				'section' => 'colors',
				'settings'   => 'color_success'
			)
		) );

		$wp_customize->add_setting( 'color_danger', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_danger',
			array(
				'label' => 'Accent Color 3',
				'description' => 'Classes: .text-danger, .bg-danger, .border-danger',
				'section' => 'colors',
				'settings' => 'color_danger'
			)
		) );

		$wp_customize->add_setting( 'color_warning', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_warning',
			array(
				'label' => 'Accent Color 4',
				'description' => 'Classes: .text-warning, .bg-warning, .border-warning',
				'section' => 'colors',
				'settings' => 'color_warning'
			)
		) );

		$wp_customize->add_setting( 'color_info', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_info',
			array(
				'label' => 'Accent Color 5',
				'description' => 'Classes: .text-info, .bg-info, .border-info',
				'section' => 'colors',
				'settings' => 'color_info'
			)
		) );

		$wp_customize->add_setting( 'color_muted', array(
			'default' => '',
			'type' => 'theme_mod',
			'transport' => 'refresh',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new Customize_Alpha_Color_Control(
			$wp_customize,
			'color_muted',
			array(
				'label' => 'Accent Color 6',
				'description' => 'Class: .text-muted',
				'section' => 'colors',
				'settings' => 'color_muted'
			)
		) );

		// End Colors
		// --------------------------------------------------------------

		// Header
		$wp_customize->add_section( 'understrap_header', array(
			'title' => 'Header',
			'capability' => 'edit_theme_options',
			'description' => '',
			'priority' => 30
		) );

		$wp_customize->add_setting( 'understrap_site_logo', array(
			'default' => '',
			'sanitize_callback' => 'esc_url_raw'
		));
	 
		$wp_customize->add_control( new WP_Customize_Image_Control( 
			$wp_customize,
			'understrap_site_logo',
			array(
				'label' => 'Upload Logo',
				'section' => 'understrap_header',
				'settings' => 'understrap_site_logo',
				'button_labels' => array(
					'select' => 'Select Logo',
					'remove' => 'Remove Logo',
					'change' => 'Change Logo'
				)
			)
		) );

		// Header Type
		$wp_customize->add_setting( 'understrap_header_style', array(
			'default' => 'inline',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_style',
			array(
				'label' => 'Header Style',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_style',
				'type' => 'select',
				'choices' => array(
					'inline' => 'Inline',
					'multirow' => 'Multirow'
				)
			)
		) );

		// Header Stickiness
		$wp_customize->add_setting( 'understrap_header_stickiness', array(
			'default' => 'normal',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_stickiness',
			array(
				'label' => 'Header Stickiness',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_stickiness',
				'type' => 'select',
				'choices' => array(
					'normal' => 'Normal',
					'sticky-top' => 'Sticky'
				)
			)
		) );

		// Header BG Color
		$wp_customize->add_setting( 'understrap_header_bg_color', array(
			'default' => 'bg-light',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_bg_color',
			array(
				'label' => 'Header Background Color',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_bg_color',
				'type' => 'select',
				'choices' => array(
					'bg-white' => 'White',
					'bg-primary' => 'Primary',
					'bg-secondary' => 'Secondary',
					'bg-light' => 'Accent color 1',
					'bg-success' => 'Accent Color 2',
					'bg-danger' => 'Accent Color 3',
					'bg-warning' => 'Accent Color 4',
					'bg-info' => 'Accent Color 5',
					'bg-muted' => 'Accent Color 6'
				)
			)
		) );

		$wp_customize->add_setting( 'understrap_header_logo_alignment', array(
			'default' => 'center',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_logo_alignment',
			array(
				'label' => 'Multirow Logo Alignment',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_logo_alignment',
				'type' => 'select',
				'choices' => array(
					'start' => 'Left',
					'center' => 'Center',
					'end' => 'Right'
				)
			)
		) );

		$wp_customize->add_setting( 'understrap_header_modal_button_color', array(
			'default' => 'navbar-dark',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_modal_button_color',
			array(
				'label' => 'Multirow Nav Button Color',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_modal_button_color',
				'type' => 'select',
				'choices' => array(
					'navbar-light' => 'Dark',
					'navbar-dark' => 'Light'
				)
			)
		) );

		$wp_customize->add_setting( 'understrap_header_nav_placement', array(
			'default' => 'above',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_nav_placement',
			array(
				'label' => 'Multirow Navigation Placement',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_nav_placement',
				'type' => 'select',
				'choices' => array(
					'above' => 'Above Logo',
					'under' => 'Under Logo'
				)
			)
		) );

		// Hide Primary Navigation at...
		$wp_customize->add_setting( 'understrap_header_nav_breakpoint', array(
			'default' => 'xl',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_nav_breakpoint',
			array(
				'label' => 'Primary Navigation Visibility',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_nav_breakpoint',
				'type' => 'select',
				'description' => 'Show the primary navigation only at the selected minimum width',
				'choices' => array(
					'xl' => '&geq; 1200 pixels',
					'lg' => '&geq; 992 pixels',
					'md' => '&geq; 768 pixels',
					'sm' => '&geq; 576 pixels'
				)
			)
		) );

		// Icon for the modal
		$wp_customize->add_setting( 'understrap_header_modal_icon', array(
			'default' => 'fa-search',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_header_modal_icon',
			array(
				'label' => 'Icon Next to Primary Navigation',
				'section' => 'understrap_header',
				'settings' => 'understrap_header_modal_icon',
				'type' => 'select',
				'description' => 'Select which icon to show when the primary navigation is visible',
				'choices' => array(
					'fa-search' => 'Search',
					'fa-bars' => 'Bars'
				)
			)
		) );

		// Page Title BG Color
		$wp_customize->add_setting( 'understrap_page_title_bg_color', array(
			'default' => 'bg-light',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_page_title_bg_color',
			array(
				'label' => 'Page Title Background Color',
				'section' => 'understrap_header',
				'settings' => 'understrap_page_title_bg_color',
				'type' => 'select',
				'choices' => array(
					'bg-white' => 'White',
					'bg-primary' => 'Primary',
					'bg-secondary' => 'Secondary',
					'bg-light' => 'Accent color 1',
					'bg-success' => 'Accent Color 2',
					'bg-danger' => 'Accent Color 3',
					'bg-warning' => 'Accent Color 4',
					'bg-info' => 'Accent Color 5',
					'bg-muted' => 'Accent Color 6'
				)
			)
		) );

		// Page Title BG Color
		$wp_customize->add_setting( 'understrap_show_breadcrumbs', array(
			'default' => 'yes',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_show_breadcrumbs',
			array(
				'label' => 'Show Breadcrumbs',
				'section' => 'understrap_header',
				'settings' => 'understrap_show_breadcrumbs',
				'type' => 'select',
				'choices' => array(
					'yes' => 'Yes',
					'no' => 'No'
				)
			)
		) );

		// End Header
		// --------------------------------------------------------------

		// Menus
		// Adjust priority of Menus section (to keep order)
		if ( !empty( $wp_customize->get_panel( 'nav_menus' ) ) ) {
			$wp_customize->get_panel( 'nav_menus' )->priority = 40;
		}

		// Remove redundant section
		if ( !empty( $wp_customize->get_section( 'menu_locations' ) ) ) {
			$wp_customize->remove_section( 'menu_locations' );
		}

		$wp_customize->add_section( 'understrap_nav_styles_and_features', array(
			'title' => 'Menu Colors',
			'panel' => 'nav_menus',
			'capability' => 'edit_theme_options',
			'priority' => -1
		) );

		$wp_customize->add_setting( 'understrap_primary_nav_bg_color', array(
			'default' => 'bg-light',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_primary_nav_bg_color',
			array(
				'label' => 'Nav Background Color (Multirow)',
				'section' => 'understrap_nav_styles_and_features',
				'settings' => 'understrap_primary_nav_bg_color',
				'type' => 'select',
				'choices' => array(
					'bg-primary' => 'Primary',
					'bg-secondary' => 'Secondary',
					'bg-white' => 'White',
					'bg-light' => 'Accent color 1',
					'bg-success' => 'Accent Color 2',
					'bg-danger' => 'Accent Color 3',
					'bg-warning' => 'Accent Color 4',
					'bg-info' => 'Accent Color 5',
					'bg-muted' => 'Accent Color 6'
				)
			)
		) );
		
		$wp_customize->add_setting( 'understrap_primary_nav_color_scheme', array(
			'default' => 'navbar-light',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_primary_nav_color_scheme',
			array(
				'label' => 'Nav Link Color',
				'section' => 'understrap_nav_styles_and_features',
				'settings' => 'understrap_primary_nav_color_scheme',
				'type' => 'select',
				'choices' => array(
					'navbar-light' => 'Dark',
					'navbar-dark' => 'Light'
				)
			)
		) );

		// End Menus
		// --------------------------------------------------------------

		// Search Modal
		$wp_customize->add_section( 'understrap_search_modal', array(
			'title' => 'Search Modal',
			'capability' => 'edit_theme_options',
			'priority' => 50
		) );

		$wp_customize->add_setting( 'understrap_search_url', array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_search_url',
			array(
				'label' => 'Target URL',
				'description' => 'Specify the URL that should be assigned as the form\'s ACTION attribute',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_search_url',
				'type' => 'url'
			)
		) );

		$wp_customize->add_setting( 'understrap_search_input_name', array(
			'default' => 's',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_search_input_name',
			array(
				'label' => 'Name',
				'description' => 'Customize the search field name attribute',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_search_input_name',
				'type' => 'text'
			)
		) );

		// Hide Modal Navigation at...
		$wp_customize->add_setting( 'understrap_modal_nav_breakpoint', array(
			'default' => 'xl',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_modal_nav_breakpoint',
			array(
				'label' => 'Modal Navigation Visibility',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_modal_nav_breakpoint',
				'type' => 'select',
				'description' => 'Show the search modal navigation only at the selected maximum width',
				'choices' => array(
					'al' => 'Always',
					'xl' => '&leq; 1200 pixels',
					'lg' => '&leq; 992 pixels',
					'md' => '&leq; 768 pixels',
					'sm' => '&leq; 576 pixels'
				)
			)
		) );

		// Which Nav to use for the modal
		$wp_customize->add_setting( 'understrap_modal_nav', array(
			'default' => 'modal',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_modal_nav',
			array(
				'label' => 'Modal Navigation',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_modal_nav',
				'type' => 'select',
				'description' => 'Select which navigation to show in the search modal',
				'choices' => array(
					'primary' => 'Primary',
					'modal' => 'Modal',
					'both-p-m' => 'Both (Primary/Modal)',
					'both-m-p' => 'Both (Modal/Primary)'
				)
			)
		) );

		$wp_customize->add_setting( 'understrap_search_custom_input_fields', array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_search_custom_input_fields',
			array(
				'label' => 'Custom HTML',
				'description' => 'Enter any custom HTML needed by the search provider',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_search_custom_input_fields',
				'type' => 'textarea',
				'priority'    => '60'
			)
		) );

		$wp_customize->add_setting( 'understrap_search_bg_color', array(
			'default' => '',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_search_bg_color',
			array(
				'label' => 'Modal Background Color',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_search_bg_color',
				'type' => 'select',
				'choices' => array(
					'' => 'Default (Dark)',
					'bg-white' => 'White',
					'bg-primary' => 'Primary',
					'bg-secondary' => 'Secondary',
					'bg-light' => 'Accent color 1',
					'bg-success' => 'Accent Color 2',
					'bg-danger' => 'Accent Color 3',
					'bg-warning' => 'Accent Color 4',
					'bg-info' => 'Accent Color 5'
				),
				'priority' => '70'
			)
		) );

		$wp_customize->add_setting( 'understrap_search_link_color', array(
			'default' => 'text-primary',
			'type' => 'theme_mod',
			'sanitize_callback' => 'understrap_theme_slug_sanitize_select',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_search_link_color',
			array(
				'label' => 'Modal Link Color',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_search_link_color',
				'type' => 'select',
				'choices' => array(
					'text-primary' => 'Primary',
					'text-secondary' => 'Secondary',
					'text-light' => 'Accent color 1',
					'text-success' => 'Accent Color 2',
					'text-danger' => 'Accent Color 3',
					'text-warning' => 'Accent Color 4',
					'text-info' => 'Accent Color 5'
				),
				'priority' => '80'
			)
		) );

		$wp_customize->add_setting( 'understrap_search_classes', array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_search_classes',
			array(
				'label' => 'Extra CSS Classes',
				'section' => 'understrap_search_modal',
				'settings' => 'understrap_search_classes',
				'type' => 'text',
				'priority'    => '90'
			)
		) );

		// End Search Fields
		// --------------------------------------------------------------

		// Breadcrumbs
		$wp_customize->add_section( 'understrap_breadcrumbs', array(
			'title' => 'Breadcrumbs on Archive Pages',
			'capability' => 'edit_theme_options',
			'priority' => 60
		) );

		$custom_post_types = array_diff( get_post_types( array( 'public'   => true ), 'names' ), array( 'post', 'page', 'attachment' ) );
		foreach ( $custom_post_types as $a_cpt ) {
			$wp_customize->add_setting( 'understrap_breadcrumbs_landing_' . $a_cpt, array(
				'default' => 0,
				'type' => 'theme_mod',
				'capability' => 'edit_theme_options'
			) );
			$wp_customize->add_control( new WP_Customize_Control(
				$wp_customize,
				'understrap_breadcrumbs_landing_' . $a_cpt,
				array(
					'label' => 'Base page for "' . $a_cpt . '"',
					'section' => 'understrap_breadcrumbs',
					'settings' => 'understrap_breadcrumbs_landing_' . $a_cpt,
					'type' => 'text',
					'description' => 'Page to display when a single "' . $a_cpt . '" item is selected.'
				)
			) );
		}

		$wp_customize->add_setting( 'understrap_breadcrumbs_categories', array(
			'default' => 0,
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'description' => 'Add a link to the post category in the breadcrumbs for a single post.'
		) );
		$wp_customize->add_control( new WP_Customize_Control(
			$wp_customize,
			'understrap_breadcrumbs_categories',
			array(
				'label' => 'Display Post Categories',
				'section' => 'understrap_breadcrumbs',
				'settings' => 'understrap_breadcrumbs_categories',
				'type' => 'checkbox'
			)
		) );

		// End Breadcrumbs
		// --------------------------------------------------------------

		// Additional HTML to be added to all the pages on the website
		$wp_customize->add_section( 'understrap_additional_html', array(
			'title' => 'Additional HTML',
			'capability' => 'edit_theme_options',
			'description' => 'Provide any additional HTML that should be added to all the pages (Google Analytics, custom libraries, etc).',
			'priority' => 250
		) );

		$wp_customize->add_setting( 'understrap_additional_head_code', array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Code_Editor_Control(
			$wp_customize,
			'understrap_additional_head_code',
			array(
				'label' => 'HTML Code inside <HEAD> tag',
				'section' => 'understrap_additional_html',
				'settings' => 'understrap_additional_head_code'
			)
		) );

		$wp_customize->add_setting( 'understrap_additional_html_code', array(
			'default' => '',
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options'
		) );
		$wp_customize->add_control( new WP_Customize_Code_Editor_Control(
			$wp_customize,
			'understrap_additional_html_code',
			array(
				'label' => 'HTML Code before </BODY> tag',
				'section' => 'understrap_additional_html',
				'settings' => 'understrap_additional_html_code'
			)
		) );

		// Remove unnecessary default sections
		$wp_customize->remove_section( 'title_tagline' );
		$wp_customize->remove_section( 'background_image' );
		
	}
} // endif function_exists( 'understrap_theme_customize_register' ).
add_action( 'customize_register', 'understrap_theme_customize_register', 100 );

/**
 * Select sanitization function
 *
 * @param string               $input   Slug to sanitize.
 * @param WP_Customize_Setting $setting Setting instance.
 * @return string Sanitized slug if it is a valid choice; otherwise, the setting default.
 */
if ( ! function_exists( 'understrap_theme_slug_sanitize_select' ) ) {
	function understrap_theme_slug_sanitize_select( $input, $setting ){

		// Ensure input is a slug (lowercase alphanumeric characters, dashes and underscores are allowed only).
		$input = sanitize_key( $input );

		// Get the list of possible select options.
		$choices = $setting->manager->get_control( $setting->id )->choices;

		// If the input is a valid key, return it; otherwise, return the default.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                
	}
}

/**
 * Hide unnecessary Appearance submenus (as they are already available in the Customizer)
 */
if ( ! function_exists( 'understrap_hide_appearance_submenus' ) ) {
	function understrap_hide_appearance_submenus() {
		remove_submenu_page( 'themes.php', 'widgets.php' );
		remove_submenu_page( 'themes.php', 'nav-menus.php' );
		remove_submenu_page( 'themes.php', 'background-i' );

		// Ugly way to hide the Background submenu
		if ( isset( $GLOBALS[ 'submenu' ][ 'themes.php' ] ) ) {
			foreach( $GLOBALS[ 'submenu' ][ 'themes.php' ] as $menu_index => $theme_menu ) {
				if ( $theme_menu[ 0 ] == 'Header' || $theme_menu[ 0 ] == 'Background' ) {
					unset( $GLOBALS[ 'submenu' ][ 'themes.php' ][ $menu_index ] );
				}
			}
		}
	}
}
add_action( 'admin_menu', 'understrap_hide_appearance_submenus', 999 );

/**
 * Alpha Color Picker Customizer Control
 *
 * This control adds a second slider for opacity to the stock WordPress color picker,
 * and it includes logic to seamlessly convert between RGBa and Hex color values as
 * opacity is added to or removed from a color.
 *
 * This Alpha Color Picker is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this Alpha Color Picker. If not, see <http://www.gnu.org/licenses/>.
 */

if ( class_exists( 'WP_Customize_Control' ) ) {
	class Customize_Alpha_Color_Control extends WP_Customize_Control {

		/**
		 * Official control name.
		 */
		public $type = 'alpha-color';

		/**
		 * Add support for palettes to be passed in.
		 *
		 * Supported palette values are true, false, or an array of RGBa and Hex colors.
		 */
		public $palette;

		/**
		 * Add support for showing the opacity value on the slider handle.
		 */
		public $show_opacity;

		/**
		 * Render the control.
		 */
		public function render_content() {

			// Process the palette
			if ( is_array( $this->palette ) ) {
				$palette = implode( '|', $this->palette );
			} else {
				// Default to true.
				$palette = ( false === $this->palette || 'false' === $this->palette ) ? 'false' : 'true';
			}

			// Support passing show_opacity as string or boolean. Default to true.
			$show_opacity = ( false === $this->show_opacity || 'false' === $this->show_opacity ) ? 'false' : 'true';

			// Begin the output. ?>
			<label>
				<?php // Output the label and description if they were passed in.
				if ( isset( $this->label ) && '' !== $this->label ) {
					echo '<span class="customize-control-title">' . sanitize_text_field( $this->label ) . '</span>';
				}
				if ( isset( $this->description ) && '' !== $this->description ) {
					echo '<span class="description customize-control-description">' . sanitize_text_field( $this->description ) . '</span>';
				} ?>
				<!-- <input class="alpha-color-control" type="text" data-show-opacity="<?php echo $show_opacity; ?>" data-palette="<?php echo esc_attr( $palette ); ?>" data-default-color="<?php echo esc_attr( $this->settings['default']->default ); ?>" <?php $this->link(); ?>  /> -->
				<div class="customize-control-content">
					<label>
						<input class="alpha-color-control" type="text" data-show-opacity="<?php echo $show_opacity; ?>" data-palette="<?php echo esc_attr( $palette ); ?>" data-default-color="<?php echo esc_attr( $this->settings['default']->default ); ?>" <?php $this->link(); ?>  />
					</label>
				</div>
			</label>
			<?php
		}
	}
}