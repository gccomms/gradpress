<?php
/**
 * Search button and modal.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Adds a search button/field to the navigation bar
 */

if ( !function_exists( 'understrap_get_modal_button' ) ) {
	function understrap_get_modal_button( $fa_icon = 'fa-search' ) {
		return '<a href="#modal-search" class="nav-item nav-link navbar-text bg-transparent border-0 pr-0 fa ' . $fa_icon . '" data-target="#modal-search" data-toggle="modal" type="button" aria-haspopup="true"><span class="sr-only">Open Menu and Search Dialog</span></a>';
	}
}

add_action( 'wp_footer', 'understrap_search_modal' );
if ( !function_exists( 'understrap_search_modal' ) ) {
	function understrap_search_modal() {
		$search_url = get_theme_mod( 'understrap_search_url', '' );
		$earch_input_name = get_theme_mod( 'understrap_search_input_name', 's' );
		$modal_nav_breakpoint = get_theme_mod( 'understrap_modal_nav_breakpoint', 'xl' );
		$modal_nav = get_theme_mod( 'understrap_modal_nav', 'modal' );

		$search_custom_input_fields = get_theme_mod( 'understrap_search_custom_input_fields', '' );
		$search_classes = get_theme_mod( 'understrap_search_classes', '' );
		$search_bg_color = get_theme_mod( 'understrap_search_bg_color', '' );
		$search_link_color = get_theme_mod( 'understrap_search_link_color', '' );

		if ( !empty( $search_classes ) ) {
			$search_classes = ' ' . $search_classes;
		}

		echo '
		<div class="modal fade ' . $search_link_color . ' ' . $search_bg_color . ' ' . $search_classes . '" id="modal-search" role="dialog" tabindex="-1" aria-hidden="true">
			<div class="modal-dialog container" role="document">
				<div class="modal-content bg-transparent border-0">
					<div class="modal-body p-0 ">
						<div class="row mb-3">
							<div class="col-12">
								<form class="input-group col-12 py-4 px-0" action="' . $search_url . '">
									<label for="search-input" class="d-none">Type your search keyword</label>
									<input autocomplete="off" class="form-control py-4 bg-transparent text-white border-top-0 border-right-0 border-left-0 border" type="search" placeholder="Search" id="search-input" name="' . $earch_input_name . '">
									<span class="input-group-append">
										<button class="btn bg-transparent text-white border-top-0 border-right-0 border-left-0 border fa fa-search" type="submit">
											<span class="sr-only">Submit</span>
										</button>
									</span>
									' . $search_custom_input_fields . '
								</form>
							</div>
						</div>';

						if ( has_nav_menu( 'primary' ) && ( $modal_nav == 'primary' || $modal_nav == 'both-p-m' ) ) {
							echo '
							<nav role="navigation" class="navbar justify-content-center px-0 d-' . $modal_nav_breakpoint . '-none" id="navbar-primary">
							' . wp_nav_menu( array(
								'theme_location'    => 'primary',
								'depth'             => 2,
								'container'         => 'div',
								'container_class'	=> 'menu-modal-navigation-container',
								'menu_class'        => 'nav navbar-nav',
								'fallback_cb'       => 'Understrap_WP_Bootstrap_Navwalker::fallback',
								'walker'            => new Understrap_WP_Bootstrap_Navwalker(),
								'echo'				=> false
							) ) . '
							</nav>';
						}

						if ( has_nav_menu( 'modal' ) && $modal_nav != 'primary' ) {
							echo '
							<nav role="navigation" class="navbar justify-content-center px-0 d-' . $modal_nav_breakpoint . '-none" id="navbar-modal">
							' . wp_nav_menu( array(
								'theme_location'    => 'modal',
								'depth'             => 2,
								'container'         => 'div',
								'container_class'	=> 'menu-modal-navigation-container',
								'menu_class'        => 'nav navbar-nav',
								'fallback_cb'       => 'Understrap_WP_Bootstrap_Navwalker::fallback',
								'walker'            => new Understrap_WP_Bootstrap_Navwalker(),
								'echo'				=> false
							) ) . '
							</nav>';
						}

						if ( has_nav_menu( 'primary' ) && $modal_nav == 'both-m-p' ) {
							echo '
							<nav role="navigation" class="navbar justify-content-center px-0 d-' . $modal_nav_breakpoint . '-none" id="navbar-primary">
							' . wp_nav_menu( array(
								'theme_location'    => 'primary',
								'depth'             => 2,
								'container'         => 'div',
								'container_class'	=> 'menu-modal-navigation-container',
								'menu_class'        => 'nav navbar-nav',
								'fallback_cb'       => 'Understrap_WP_Bootstrap_Navwalker::fallback',
								'walker'            => new Understrap_WP_Bootstrap_Navwalker(),
								'echo'				=> false
							) ) . '
							</nav>';
						}

						echo '
					</div>
				</div>
			</div>
		</div>';
	}
}