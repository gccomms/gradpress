<?php
/**
 * Check and setup theme's default settings
 *
 * @package understrap
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Page Properties
add_action( 'add_meta_boxes', 'gradpress_add_page_metaboxes', 10, 2 );
if ( !function_exists( 'gradpress_add_page_metaboxes' ) ) {
	function gradpress_add_page_metaboxes( $post_type ) {
		if ( 'page' === $post_type ) {
			add_meta_box(
				'gradpress_page_metabox',
				'GradPress Page Settings',
				'gradpress_page_metabox_html',
				$post_type,
				'normal',
				'low',
				$post_type
			);

			// No featured image on pages
			remove_meta_box( 'postimagediv', 'page', 'side' );
		}
	}
}

if ( !function_exists( 'gradpress_page_metabox_html' ) ) {
	function gradpress_page_metabox_html( $post_type ) {
		$page_settings_values = get_post_meta( $GLOBALS[ 'post' ]->ID, '_gradpress_page_settings', true );
		$page_settings_defaults = array(
			'is_minisite_home' => false,
			'hide_in_contextual_nav' => false,
			'hide_page_title' => false,
			'hide_breadcrumbs' => false,
			'page_title_mb_0' => false,
			'hide_header' => false,
			'hide_footer' => false,
			'page_container_type' => '',
			'redirect_url' => '',
			'custom_header_id' => '',
			'custom_footer_id' => '',
			'head_markup' => "\n\n\n",
			'footer_markup' => "\n\n\n"
		);
        if ( !is_array( $page_settings_values ) || empty( $page_settings_values ) ) {
            $page_settings_values = $page_settings_defaults;
		}
		else {
			$page_settings_values = $page_settings_values + $page_settings_defaults;
		}

		$page_settings_fields = array(
			'is_minisite_home' => array( 'checkbox', 'Minisite Home', 'col-3', '' ),
			'hide_in_contextual_nav' => array( 'checkbox', 'Hide in Navigation', 'col-3', '' ),
			'hide_header' => array( 'checkbox', 'Hide Header', 'col-3', '' ),
			'hide_page_title' => array( 'checkbox', 'Hide Title Bar', 'col-3', '' ),
			'hide_breadcrumbs' => array( 'checkbox', 'Hide Breadcrumbs', 'col-3', '' ),
			'page_title_mb_0' => array( 'checkbox', 'No Title Margin Bottom', 'col-3', '' ),
			'hide_footer' => array( 'checkbox', 'Hide Footer', 'col-3', '' ),
			'page_container_type' => array( 'radio', array( 'Page Width', array( '' => 'Default', 'container' => 'Fixed Width', 'container-fluid' => 'Full Width' ) ), 'col-6', '' ),
			'redirect_url' => array( 'text', 'Redirect URL', 'col-12', '' ),
			'custom_header_id' => array( 'text', 'Custom Header Page ID:', 'col-6', '' ),
			'custom_footer_id' => array( 'text', 'Custom Footer Page ID:', 'col-6', '' ),
			'head_markup' => array( 'textarea', 'This HTML code will be added to the <code>&lt;HEAD&gt;</code> tag.', 'col-12', 'enable-code-editor' ),
			'footer_markup' => array( 'textarea', 'This HTML code will be appended right before the closing <code>&lt;BODY&gt;</code> tag.', 'col-12', 'enable-code-editor' )
		);

		foreach ( $page_settings_fields as $a_field_id => $a_field ) {
			gradpress_display_option( $a_field_id, $a_field[ 0 ], $a_field[ 1 ], $page_settings_values[ $a_field_id ], $a_field[ 2 ], $a_field[ 3 ] );
		}
    }
}

if ( !function_exists( 'gradpress_display_option' ) ) {
	function gradpress_display_option( $_id = 'field_id', $_field_type = '', $_description = '', $_value = '', $_size = '', $_extra_field_classes = '' ) {
		echo '<p' . ( !empty( $_size ) ? ' class="' . $_size .'"' : '' ) . '><label for="gradpress_' . $_id . '" class="wide">';

		$_extra_field_classes = !empty( $_extra_field_classes ) ? ' class="' . $_extra_field_classes . '"' : '';

		switch ( $_field_type ) {
			case 'checkbox':
				echo '<input type="checkbox" id="gradpress_' . $_id . '" name="gradpress_page_settings[' . $_id . ']" ' . checked( $_value, 'on', false ) . $_extra_field_classes . '/>' . $_description;
				break;

			case 'radio':
				echo $_description[ 0 ] . ': ';
				foreach ( $_description[ 1 ] as $a_key => $a_descr ) {
					echo '<input type="radio" id="gradpress_' . $_id . $a_key . '" name="gradpress_page_settings[' . $_id . ']" value="' . $a_key . '" ' . checked( $_value, $a_key, false ) . $_extra_field_classes . '/>' . $a_descr . '&nbsp;&nbsp;&nbsp;';
				}
				break;

			case 'select':
				echo $_description[ 0 ] . '<select id="gradpress_' . $_id . '" name="gradpress_page_settings[' . $_id . ']">';
				foreach ( $_description[ 1 ] as $a_key => $a_descr ) {
					echo '<option value="' . $a_key . '"' . selected( $a_key, $_value ) . '>' . $a_descr;
				}
				echo '</select>';
				break;

			case 'textarea':
				echo $_description . '<textarea id="gradpress_' . $_id . '" rows="5" name="gradpress_page_settings[' . $_id . ']" ' . $_extra_field_classes . '>' . $_value . '</textarea>';
				break;

			default:
				echo $_description . '<input type="text" id="gradpress_' . $_id . '" name="gradpress_page_settings[' . $_id . ']" ' . $_extra_field_classes . ' value="' . $_value . '">';
				break;
		}

		echo '</label></p>';
	}
}

add_action( 'save_post', 'save_gradpress_meta_box_data' );
if ( !function_exists( 'save_gradpress_meta_box_data' ) ) {
    function save_gradpress_meta_box_data( $_post_id ) {
        // If this is an autosave, our form has not been submitted, so we don't want to do anything.
        if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || defined( 'DOING_AJAX' ) && DOING_AJAX ) {
            return;
        }

        // Update the meta fields in the database.
        if ( !empty( $_POST[ 'gradpress_page_settings' ] ) ) {
            update_post_meta( $_post_id, '_gradpress_page_settings', $_POST[ 'gradpress_page_settings' ] );
        }
    }
}