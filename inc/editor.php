<?php
/**
 * Understrap modify editor
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Remove unwanted buttons and options from TinyMCE (H1, text colors, etc)
 */
add_filter( 'mce_buttons', 'gradpress_tiny_mce_buttons', 100 );
if ( ! function_exists ( 'gradpress_tiny_mce_buttons' ) ) {
	function gradpress_tiny_mce_buttons( $buttons ) {
		// Remove the 'wp_more' button
		if ( ( $key = array_search( 'wp_more', $buttons ) ) !== false ) {
			unset( $buttons[ $key ] );
		}

		// Add a few buttons to the first row
		$buttons[] = 'removeformat';
		$buttons[] = 'pastetext';
		if ( 'page' == get_post_type() ) {
			$buttons[] = 'table';
		}
		$buttons[] = 'wp_help';

		// Remove the button to toggle the second row of buttons (advanced)
		if ( ( $key = array_search( 'wp_adv', $buttons ) ) !== false && !current_user_can( 'manage_options' ) ) {
			unset( $buttons[ $key ] );
		}
		else {
			// Move to the end of the array
			unset( $buttons[ $key ] );
			$buttons[] = 'wp_adv';
		}

		return $buttons;
	}
}

add_filter( 'mce_external_plugins', 'gradpress_tinymce_table_plugin' );
if ( ! function_exists ( 'gradpress_tinymce_table_plugin' ) ) {
	function gradpress_tinymce_table_plugin( $plugins ) {
		$plugins[ 'table' ] = get_template_directory_uri() . '/js/vendor/tinymce/plugins/table/plugin.min.js';
		return $plugins;
	}
}

add_filter( 'tiny_mce_before_init', 'gradpress_tiny_mce_before_init', 100 );
if ( ! function_exists ( 'gradpress_tiny_mce_before_init' ) ) {
	function gradpress_tiny_mce_before_init( $settings ) {
		// Allowed block formats are: H2, H3, H4 and paragraph
		$settings[ 'block_formats' ] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4';

		// Remove Rev Slider button
		$obj_settings = json_decode( $settings[ 'external_plugins' ] );
		unset( $obj_settings->revslider_sc_button );
		$settings[ 'external_plugins' ] = json_encode( $obj_settings );

		return $settings;
	}
}