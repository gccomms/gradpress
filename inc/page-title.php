<?php
/**
 * Breadcrumbs
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'gradpress_the_breadcrumbs' ) ) {
	function gradpress_the_breadcrumbs() {
		$ancestors = array();
		if ( !empty( $GLOBALS[ 'post' ] ) && ( is_single() || is_archive() ) ) {
			// Add the landing page and its ancestors to the list: we get its post ID from the Customizer
			$landing_page_id = 0;
			if ( $GLOBALS[ 'post' ]->post_type == 'post' ) {
				$landing_page_id = get_option( 'page_for_posts' ); 
			}
			else if ( $GLOBALS[ 'post' ]->post_type != 'tribe_events' || ( function_exists( 'tribe_is_past' ) && !tribe_is_past() && !tribe_is_upcoming() ) ) { // On the List View page for Events, don't add the landing page id
				$landing_page_id = get_theme_mod( 'understrap_breadcrumbs_landing_' . $GLOBALS['post']->post_type );
			}

			if ( !empty( $landing_page_id ) ) {
				$ancestors[] = $landing_page_id;
				$ancestors = array_merge( $ancestors, get_ancestors( $landing_page_id, 'page' ) );
			}
		}
		// Page
		else if ( !empty( $GLOBALS[ 'post' ]->ID ) ) {
			$ancestors = get_ancestors( $GLOBALS[ 'post' ]->ID, 'page' );
		}

		$breadcrumbs_output = array();
		$found_minisite_node = false;

		// Editors can flag a page as the 'homepage' of a minisite
		$is_this_node_minisite_home = !empty( $GLOBALS[ 'post' ]->ID ) ? get_post_meta( $GLOBALS[ 'post' ]->ID, '_gradpress_page_settings', true ) : false;
		$is_this_node_minisite_home = !empty( $is_this_node_minisite_home[ 'is_minisite_home' ] );

		foreach ( $ancestors as $id => $a_ancestor_id ) {
			if ( empty( $a_ancestor_id ) || get_the_title( $a_ancestor_id ) == '' ) {
				continue;
			}

			$is_minisite_node = get_post_meta( $a_ancestor_id, '_gradpress_page_settings', true );
			$is_minisite_node = !empty( $is_minisite_node[ 'is_minisite_home' ] );

			$breadcrumbs_output[] = '<li class="breadcrumb-item"><a href="' . get_permalink( $a_ancestor_id ) . '">' . get_the_title( $a_ancestor_id ) . '</a></li>';

			if ( !$is_this_node_minisite_home && !empty( $is_minisite_node ) ) {
				$found_minisite_node = true;
				break;
			}
		}

		if ( count( $breadcrumbs_output ) > 0 ) {
			echo '<nav aria-label="breadcrumbs"><ol class="breadcrumb">';

			if ( !$found_minisite_node ) {
				echo '<li class="breadcrumb-item"><a href="' . get_home_url() . '">Home</a></li>';
			}

			echo implode( '', array_reverse( $breadcrumbs_output ) );


			if ( is_single() ) {
				if ( $GLOBALS[ 'post' ]->post_type == 'tribe_events' && function_exists( 'tribe_get_start_time' ) ) {
					$timestamp = tribe_get_start_time( $GLOBALS[ 'post' ]->ID, 'U' );
					$month_year_link = Tribe__Events__Main::instance()->getLink( 'month', date_i18n( 'Y-m', $timestamp ) );
					$month_year_label = date_i18n( 'F Y', $timestamp );
					echo '<li class="breadcrumb-item active"><a href="' . $month_year_link . '">' . $month_year_label . '</a></li>';
				}
				else if ( $GLOBALS[ 'post' ]->post_type == 'post' ) {
					$timestamp = strtotime( $GLOBALS[ 'post' ]->post_date );
					$month_year_link = get_month_link( date_i18n( 'Y', $timestamp ), date_i18n( 'm', $timestamp ) );
					$month_year_label = date_i18n( 'F Y', $timestamp );
					echo '<li class="breadcrumb-item active"><a href="' . $month_year_link . '">' . $month_year_label . '</a></li>';
				}
			}

			echo '</ol></nav>';
		} // if ( count( $breadcrumbs_output ) > 1)

		// echo '<li class="breadcrumb-item active">' . gradpress_get_the_title() . '</li>';
	} // end function declaration
}

if ( !function_exists( 'gradpress_the_title' ) ) {
	function gradpress_the_title() {

		$mb_size = ( !empty( $GLOBALS[ 'page_settings' ][ 'page_title_mb_0' ] ) && $GLOBALS[ 'page_settings' ][ 'page_title_mb_0' ] == 'on' ) ? 'mb-0' : 'mb-4';
		$container = !empty( $GLOBALS[ 'page_settings' ][ 'page_container_type' ] ) ? $GLOBALS[ 'page_settings' ][ 'page_container_type' ] : get_theme_mod( 'understrap_container_type', 'container' );

		// Retrieve the background color for the page title from the global settings (Customizer)
		$page_title_bg_color = get_theme_mod( 'understrap_page_title_bg_color', 'bg-light' ); ?>

		<header class="entry-header <?php echo esc_attr( $mb_size ) . ' ' . esc_attr( $page_title_bg_color) ; ?> pt-2 pb-1">
			<div class="<?php echo esc_attr( $container ); ?>">
			<?php
				if ( get_theme_mod( 'understrap_show_breadcrumbs', '' ) == 'yes' && empty( $GLOBALS[ 'page_settings' ][ 'hide_breadcrumbs' ] ) ) {
					gradpress_the_breadcrumbs();
				}

				echo '<h1 class="entry-title display-1">' . gradpress_get_the_title() . '</h1>';
			?>
			</div>
		</header><!-- .entry-header -->
		<?php
	}
}

if ( !function_exists( 'gradpress_get_the_title' ) ) {
	function gradpress_get_the_title() {
		if ( empty( $GLOBALS[ 'post' ]->ID ) ) {
			return 'Nothing Found';
		}
		else if ( is_home() ) {
			// Blog posts index page
			return single_post_title( '', false );
		}
		else if ( is_archive() ) {
			if ( $GLOBALS[ 'post' ]->post_type == 'tribe_events' && function_exists( 'tribe_get_events_title' ) ) {
				return tribe_get_events_title();
			}
			else {
				return get_the_archive_title();
			}
		}
		else {
			// Single page or post
			return get_the_title();
		}
	}
}

add_filter( 'private_title_format', 'gradpress_remove_prefix_from_title' );
add_filter( 'protected_title_format', 'gradpress_remove_prefix_from_title' );
if ( !function_exists( 'gradpress_remove_prefix_from_title' ) ) {
	function gradpress_remove_prefix_from_title( $_format ) {
		return '%s';
	}
}