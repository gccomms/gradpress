# GradPress

GradPress is an opinionated [WordPress](https://wordpress.org) theme framework, loosely based on [Understrap](https://understrap.com). It combines the [Underscores starter theme](https://underscores.me/) with [Bootstrap](https://getbootstrap.com/) and [WPBakery Page Builder](https://wpbakery.com/) into a perfect open source foundation for your next WordPress theme project. GradPress is proudly maintained by the Office of Communications and Marketing at [The Graduate Center, CUNY](https://www.gc.cuny.edu/) and is released under the [MIT License](LICENSE).

## Objectives

* An intuitive and unobtrusive web interface.
* Developer-friendly folder structure.
* Modular WordPress integration via the Customizer.

## Main Features

* Utilizes the SASS sources from all assets as well as own SASS files. Makes it easy to replace colors, fonts, stylings etc.
* Comes with Gulp as task runner and one pre-made gulpfile.js with some default tasks. For example to compile SASS to CSS.
* Comes with Font Awesome icon kit built into the framework. A huge and free (MIT License) set with over 600 icons.
* Defines new Page Builder elements for better content strategy: page include, post list, header, icon, and more.

## Getting Started

You can install this theme either by [downloading the zip file](https://gitlab.com/gccomms/gradpress/-/archive/master/gradpress-master.zip) or via command line by cloning the repository under **wp-content/themes**:

`git clone -b master https://gitlab.com/gccomms/gradpress.git /var/www/html/wp-content/themes/gradpress/`

When you activate GradPress on your brand-new website, your first reaction about how your page looks might be of slight disappointment. But don't you worry: follow the steps here below, and your site's appearance will greatly improve in minutes. This tutorial assumes that you have WPBakery Page Builder installed and activated. If this is not the case, you can still follow these steps, however your mileage may vary in terms of look and feel. We also assume that your site already contains a few pages, posts and categories.

Start by creating three new pages called **Header**, **Footer** and **Search**. Using WPB Page Builder, add a new row and create the corresponding layout for what your website's header, footer and search overlay should look like (images, navigation items, icons, etc). Write down their page IDs for later.

Now, let's head over to Appearance > Customize:

1. Under `Colors` define your brand identity by assigning specific colors to the various labels (primary, secondary, accent color 1, etc). These colors can be used throughout the site by applying the corresponding class names (see descriptions under each color). For consistency sake, we used the default Bootstrap naming convention.
1. Under `Menus` create your primary navigation, by associating pages to it. Once that's done, you can either assign it to the Primary Navigation or Secondary Navigation placeholder. The former will display the navigation above the header, while the latter will be placed below it. You can use both placeholders, if needed. 
	1. Select `Styles and Features` to customize the look and feel for your navigation, by selecting the appropriate colors from the available dropdown menus.
1. Under `Widgets` assign content to your header and footer areas. 
	1. Select `Header` and click on Add a Widget. Choose `GradPress Page Include`, and enter the Header page ID you wrote down earlier. 
	1. Now go back and repeat the same steps for your `Footer`. 
	1. Please note, you don't have to use a page include widget: feel free to experiment with different layouts and combinations of widgets and HTML markup.
1. Under `Homepage Settings` identify what page on your website should be used as the homepage, and which one should display the list of news or posts.
1. Under `Layout` use the available dropdowns to customize the behavior of certain page elements. Most of them are self-explanatory.
	1. `Sidebar Positioning` determines where to display your post sidebars.
	1. `Header Stickiness` makes your header sticky (always visible).
1. Under `Search Form` choose if you would like the search field to be displayed inline inside the nav bar (primary or secondary) or as clickable magnifying glass that reveals an overlay with search field and extra links.
	1. `Target URL` is the permalink of the search results page. This can be an external resource or an internal page using Google Custom Search.
	1. `Name` is the search field's name/id attribute, which depends on the search engine you're using to index your website.
	1. Enter in `Overlay Page ID` the ID you wrote down earlier. Feel free to experiment with different layouts to understand how to use this feature.
	1. `Background color` and other fields are used to customize the look and feel of your overlay.
1. Under `Breadcrumbs` define what page to use for each dynamic post type (posts, people, books, events). For example, if you have a landing page with a list of news, that's the page ID you will want to enter in the corresponding field in this section. It will be used to generate the correct breadcrumb path when displaying a single news item.
1. Under `Additional CSS/HTML` add any HTML code or styles that should be appended to all the pages in your site (Google Analytics, other Javascript features, custom styles, etc).

Your site is now ready to be populated with pages, news, events and any other content type that suits your needs.

## Bootstrap Integration

All the Bootstrap class names are available in GradPress to customize the look and feel of your pages. Please refer to this [cheat sheet](https://www.w3schools.com/bootstrap4/bootstrap_ref_all_classes.asp) to learn more about what classes can be used in conjunction with WPBakery Page Builder.

## Customizing GradPress

If you're not using our [Graduate Vagrant](https://gitlab.com/gccomms/vagrant) virtual machine, you'll need to install [NPM](https://www.npmjs.com/get-npm) on your local machine to take advantage of the built-in Gulp *goodies* that ship with GradPress. Once you do that, here are some basic notes about the Sass and CSS files that you'll find bundled with our theme:

* The theme itself uses the `/style.css` file only to identify the theme inside of WordPress. The file is not loaded by the theme and does not include any styles.
* The main `/css/theme.css` file is generated via **gulp** by compiling, concatenating and minifying a few files. Please **do not** edit this file directly. Open a command line shell, go the the theme folder and launch `gulp watch` to start the Gulp process (if you get any error messages, make sure to install Gulp with `npm i gulp` and any other dependencies). Then edit the following Sass files:
	* `/src/sass/theme/_theme_variables.scss` - global theme variables, mainly colors
	* `/src/sass/theme/_theme.scss` - custom theme styles
	* `/src/sass/theme/_wordpress.scss` - WordPress classes (alignment, images, etc)
	* `/src/sass/admin/admin.scss` - definitions to customize the appearance of the WP admin interface

* Similarly, your Javascript code can be added to the following files:
	* `/src/js/theme/theme.js` - functions to be loaded on the front-end (minified as `/js/theme.min.js` along with all the Bootstrap goodies)
	* `/src/js/admin/admin.js` - code to be loaded in the WordPress admin (minified as `/js/admin.min.js`)

## Page Builder Elements

GradPress defines a few useful elements to streamline your content strategy:

* **Contextual Navigation**: Takes the burden off editors to display a list of pages in the current section. With this element, you will not need to create hundreds of WP menus to assign to each section of your website. Simply place this element on each page, and let it do the rest. The built-in business logic will analyze your site tree and determine what links is appropriate to display, at any depth.
* **Page Include**: Do you have snippets of content that you reuse on multiple pages? (like sidebars, contact information, etc) If you do, this element allows you to create that content once as a WP page, and then reuse it as many times as you like within other pages.
* **Post List**: Adding lists of news, events, people, books or any other custom post type you might have has never been easier. Break free from the burden of defining a page template for each layout you'd like to use, and leverage the power of WPBakery Page Builder to do that on-the-fly. This element provides many ways to filter the list of items and to style their look and feel.
* **Header**: A simple element to add headings to your content, with built-in styling features that comply with your brand identity.
* **Icon**: Add any Font Awesome icon bundled with Bootstrap to your pages.

You may wonder if GradPress will eventually support [Gutenberg](https://wordpress.org/gutenberg/)? The theme's core foundation has been built with flexibility and scalability in mind, and it can be extended to work with other page builders. However, Gutenberg still lacks many enterprise-level features available in other popular extensions, like the capability to restrict blocks by role, responsive grid-friendly rows and columns, and so on. It would be great to rely on built-in WordPress features and drop any third-party dependencies, and we will make sure to add support for Gutenberg when this page editor reaches a maturity that makes it suitable for large enterprise content management system deployments.

## Recommended Tools

In order to enhance your WordPress environment and enable features that will turn it into an enterprise content management system, we suggest installing the following software:

* [Graduate Vagrant](https://gitlab.com/gccomms/vagrant): a virtual development environment based on modern best practices.
* [DaSh](https://gitlab.com/gccomms/wordpress): a WordPress boilerplate environment that separates your code from all the necessary dependencies.
* [GC Access Control](https://gitlab.com/gccomms/gc-acl): a plugin that provides granular role and capability management, to determine which users have access to which pages on your website.
* [Nested Pages](https://gitlab.com/gccomms/wp-nested-pages): our fork of a popular plugin to better navigate your site tree, with added compatibility for GC Access Control.
* [Members](https://wordpress.org/plugins/members/): a plugin that puts you in control over your site's roles and capabilities.
* [Broken Link Checker](https://wordpress.org/plugins/broken-link-checker/): a plugin that monitors your blog looking for broken links and lets you know if any are found.

## Licenses and Credits

* [Font Awesome](http://fontawesome.io/license) - SIL OFL 1.1, CSS: MIT License
* [Bootstrap](https://github.com/twbs/bootstrap/blob/master/LICENSE) -  Code: MIT License, Documentation: CC BY 3.0
* [jQuery](https://jquery.org/license/) - Code: MIT License
* [WP Bootstrap Navwalker](https://github.com/wp-bootstrap/wp-bootstrap-navwalker/blob/master/LICENSE.txt) by Edward McIntyre - Code: GNU GPL