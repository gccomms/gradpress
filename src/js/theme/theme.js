jQuery( function( $ ) {
	var modal_trigger_object, modal_trigger_class;

	// Make dropdown links clickable
	$( 'li.dropdown > a' ).on( 'click', function( e ) {
		if ( ( $( this ).parent().hasClass( 'show' ) || $( this ).parent().is( ':hover' ) ) && typeof( e.which ) != 'undefined' ) {
			location.href = $( this ).attr( 'href' );
		}
	});

	// Accessible dropdowns should be keyboard friendly
	$( 'li.menu-item > a' ).on( 'focus', function() {
		if ( $( this ).next( 'ul.dropdown-menu' ).length ) {
			$( this ).parent().addClass( 'show' );
		}
		$( this ).parent().siblings( 'li.menu-item' ).removeClass( 'show' );
		$( this ).closest( 'ul' ).addClass( 'focus' );
	});

	// Close any open dropdowns if user is tabbing around the page
	$( 'a' ).on( 'focus', function() {
		if ( !$( this ).parents( '.navbar-nav' ).length ) {
			$( 'ul.navbar-nav li').removeClass( 'show' );
			$( 'ul.navbar-nav').removeClass( 'focus' );
		}
	});

	// Focus the search element in the modal
	$( '#modal-search' ).on( 'shown.bs.modal', function() {
        $( this ).find( '#search-input' ).trigger( 'focus' );
    });

	// Change the trigger icon as needed
	$( '#modal-search' ).on( 'show.bs.modal', function( e ) {
		$( e.relatedTarget ).fadeOut( 250 , function() {
			modal_trigger_object = $( e.relatedTarget );
			modal_trigger_class = $( e.relatedTarget ).attr( 'class' );
			$( e.relatedTarget ).removeClass( 'fa-bars' ).removeClass( 'fa-search' ).addClass( 'fa-close' ).fadeIn( 250 );			
		} );
	} );
	$( '#modal-search' ).on( 'hide.bs.modal', function( e ) {
		$( modal_trigger_object ).fadeOut( 250 , function() {
			$( modal_trigger_object ).attr( 'class', modal_trigger_class ).fadeIn( 250 );
		} );
	} );

	// Hide sticky header on scroll
	if ( $( '#wrapper-header.sticky-top' ).length ) {
		var prevScrollpos = window.pageYOffset;
		window.onscroll = function() {
			var currentScrollPos = window.pageYOffset;
			if ( prevScrollpos > currentScrollPos ) {
				$( '#wrapper-header.sticky-top' ).css( 'top', '0' );
			} else {
				$( '#wrapper-header.sticky-top' ).css( 'top', -Math.round( $( '#wrapper-header.sticky-top' ).outerHeight() ) );
			}
			prevScrollpos = currentScrollPos;
		}

		// Add the appropriate padding to the modal, since the header stays visible on top of it
		$( '.modal' ).css( 'padding-top', Math.round( $( '#wrapper-header.sticky-top' ).outerHeight() + 20 ) );
	}

	// Append file extension to downloads
	$( 'a[href$=".pdf"]:not( :has( img ) )' ).append( ' (PDF)' );
	$( 'a[href$=".doc"]:not( :has( img ) )' ).append( ' (DOC)' );
	$( 'a[href$=".docx"]:not( :has( img ) )' ).append( ' (DOC)' );
	$( 'a[href$=".xls"]:not( :has( img ) )' ).append( ' (XLS)' );
	$( 'a[href$=".xlsx"]:not( :has( img ) )' ).append( ' (XLS)' );

	// Add navigation toggle to elements with children, on mobile overlay
	$( '.modal .menu-item-has-children > a' ).after( function() {
		return '<button aria-expanded="false" class="expand-sub-navigation bg-transparent border-0 fa fa-angle-down"><span class="sr-only">Expand submenu for ' + this.innerHTML + '</span></button>'
	} );

	// Expand/Collapse Modal Sub-Navigation
	$( document ).on( 'click', '.expand-sub-navigation', function( e ) {
		e.preventDefault();
		$( this ).removeClass( 'expand-sub-navigation' ).addClass( 'collapse-sub-navigation' ).attr( 'aria-expanded', 'true' );
		$( this ).siblings( 'ul' ).slideToggle( 250 );
	} );
	$( document ).on('click', '.collapse-sub-navigation', function( e ) {
		e.preventDefault();
		$( this ).removeClass( 'collapse-sub-navigation' ).addClass( 'expand-sub-navigation' ).attr( 'aria-expanded', 'false' );
		$( this ).siblings( 'ul' ).slideToggle( 250 );
	});
	
	// Start Jarallax
	$( '[data-speed]' ).jarallax();

});

