## Release 1.5.3 - June 3, 2019
- Updated Single Image element to remove unused parameters and simplify the overall user experience.
- One of the projects mentioned in the ReadMe has been renamed to [DaSh](https://gitlab.com/gccomms/wordpress): go check it out!
- Implemented a more generic rule that prevents users who cannot delete from changing the post/page status (now applied to any custom post type)

## Release 1.5.2 - May 23, 2019
- Removed featured image metabox from Edit Page screen.
- Users who cannot delete pages now will not be allowed to change the Page status (Published -> Draft).

## Release 1.5.1 - April 16, 2019
- Fixed a bug affecting the Post List element, where all posts would be displayed if the "Network" blog was fetching from an empty taxonomy.
- Added '.vscode' to .gitignore.
- Extended Post List element to allow for more flexibility in formatting the output.

## Release 1.5 - April 9, 2019
- GradPress now works with the [Frontend Editor](https://wpbakery.com/features/wordpress-editor/) available in WPBakery Page Builder. This feature enables an even more intuitive editing experience for your editors.
- Consolidated hook definitions.
- Updated `gulpfile.js` to process the new frontend editor CSS file, which loads all the font icons for the live editor.

## Release 1.4.1 - March 20, 2019
- Introduced new CSS rules for aligning elements correctly.
- Updated events calendar templates to honor the container width settings (Customizer).
- Updated npm dependencies gulp-uglify and gulp-sourcemaps to the latest version available.
- Removed unneeded code from home.php template.

## Release 1.4 - February 28, 2019
- Added new features to the row element, to remove dependencies and rely just on built-in Bootstrap features.
- Updated Bootstrap to [version 4.3.1](https://github.com/twbs/bootstrap/releases/tag/v4.3.1).
- Added [Jarallax library](https://github.com/nk-o/jarallax) as a dependency to support background parallax effects on rows.
- New improved `gulpfile.js`, which gets rid of a few confusing tasks and definitions.
- Removed some unneeded dependencies from `package.json`.
- Further changes to the folder structure, please refer to the [CHANGELOG](CHANGELOG) for more information.

## Release 1.3.2 - February 14, 2019
- Refactored layout management features (Customizer).
- Imported WPBakery FrontEnd JS into custom-javascript, and tweaked code to make it compatible with Bootstrap features.
- Implemented new simplified "row" parameters (background color, width).
- Implemented video background feature.

## Release 1.3.1 - February 11, 2019
- Added support for RGBa colors to the Customizer (kudos to [Braad Martin](https://github.com/BraadMartin/components/tree/master/customizer/alpha-color-picker)).
- Updated gulpfile.js to add admin-style.css to the watcher task.
- Simplified HTML output: less unnecessary nested DIVs, more intuitive class names.
- Dequeued WPBakery Frontend CSS for faster loading, as we rely on Bootstrap.
- Restructured folder organization to remove duplication and make it more intuitive to understand where things are.

## Release 1.3 - February 8, 2019
- Added new option to Customizer to allow administrators to change layout width (fixed/full).
- Renamed Settings key in the database from understrap_page_settings to gradpress_page_settings.
- Layout width can now be overridden at the page level.
- Removed search.php template for the time being.
- Updated CodeMirror init function to be more generic.

## Release 1.2 - Jan 31, 2019
- Merged upstream changes from Understrap 0.8.9.
- Added new Visual Composer elements: Page Include and Post List.
- Consolidated stylesheets (both admin and public site).