<?php
/**
 * Right sidebar check.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

echo '</div></div><!-- #closing the primary column container (and inner DIV) from /sidebar-templates/sidebar-left.php -->';

if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) {
	echo '<div class="col-md-3 widget-area" id="right-sidebar" role="complementary"><div class="vc_column-inner">';
	dynamic_sidebar( 'right-sidebar' );
	echo '</div></div><!-- #right-sidebar -->';
}