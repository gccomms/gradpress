<?php
/**
 * Left sidebar check.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>
<?php
if ( 'left' === $sidebar_pos || 'both' === $sidebar_pos ) {
	echo '<div class="col-md-3 widget-area" id="left-sidebar" role="complementary">';
	dynamic_sidebar( 'left-sidebar' );
	echo '</div><!-- #left-sidebar -->';
}

$html = '';
if ( 'right' === $sidebar_pos || 'left' === $sidebar_pos ) {
	$html = '<div class="';
	if ( ( is_active_sidebar( 'right-sidebar' ) && 'right' === $sidebar_pos ) || ( is_active_sidebar( 'left-sidebar' ) && 'left' === $sidebar_pos ) ) {
		$html .= 'col-md-9 content-area" id="primary">';
	} else {
		$html .= 'col-md-12 content-area" id="primary">';
	}
	echo $html; // WPCS: XSS OK.
}
elseif ( 'both' === $sidebar_pos ) {
	$html = '<div class="';
	if ( is_active_sidebar( 'right-sidebar' ) && is_active_sidebar( 'left-sidebar' ) ) {
		$html .= 'col-md-6 content-area" id="primary">';
	} elseif ( is_active_sidebar( 'right-sidebar' ) || is_active_sidebar( 'left-sidebar' ) ) {
		$html .= 'col-md-9 content-area" id="primary">';
	} else {
		$html .= 'col-md-12 content-area" id="primary">';
	}
	echo $html; // WPCS: XSS OK.
}
else {
	echo '<div class="col-md-12 content-area" id="primary">';
}

echo '<div class="vc_column-inner">';
