<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version 4.6.19
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$container = get_theme_mod( 'understrap_container_type', 'container' );

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

$event_id = get_the_ID();

gradpress_the_title();

while ( have_posts() ) :  the_post(); ?>
	<div class="<?php echo esc_attr( $container ); ?>"><div class="row row-o-equal-height">
		<div class="col-9">
			<div class="vc_column-inner">
				<div class="wpb_text_column wpb_content_element">
					<?php
						// Event featured image, but exclude link
						echo tribe_event_featured_image( $event_id, 'full', false );
						
						// Event content
						do_action( 'tribe_events_single_event_before_the_content' );
						the_content();

						do_action( 'tribe_events_single_event_after_the_content' );
					?>
				</div>
			</div>
		</div>
		<div class="col-3 bg-light">
			<div class="vc_column-inner">
				<?php tribe_the_notices(); ?>
				
				<div class="wpb_text_column wpb_content_element bg-light">
					<?php
						// // Event meta
						do_action( 'tribe_events_single_event_before_the_meta' );
						tribe_get_template_part( 'modules/meta' );
						// This action displays related events, which we moved to another location
						// do_action( 'tribe_events_single_event_after_the_meta' );
					?>
				</div>
			</div>
		</div>
	</div></div>
	<div class="<?php echo esc_attr( $container ); ?>"><div class="row row-o-equal-height">
		<div class="col-12">
			<div class="vc_column-inner">
				<div class="wpb_text_column wpb_content_element">
					<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
				</div>
			</div>
		</div>
	</div></div>
<?php endwhile;