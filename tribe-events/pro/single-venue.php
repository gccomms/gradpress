<?php
/**
 * Single Venue Template
 * The template for a venue. By default it displays venue information and lists
 * events that occur at the specified venue.
 *
 * This view contains the filters required to create an effective single venue view.
 *
 * You can recreate an ENTIRELY new single venue view by doing a template override, and placing
 * a single-venue.php file in a tribe-events/pro/ directory within your theme directory, which
 * will override the /views/pro/single-venue.php.
 *
 * You can use any or all filters included in this file or create your own filters in
 * your functions.php. In order to modify or extend a single filter, please see our
 * readme on templates hooks and filters (TO-DO)
 *
 * @package TribeEventsCalendarPro
 *
 * @version 4.4.28
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! $wp_query = tribe_get_global_query_object() ) {
    return;
}

$venue_id = get_the_ID();
$full_address = tribe_get_full_address();
$telephone = tribe_get_phone();
$container = get_theme_mod( 'understrap_container_type', 'container' );

gradpress_the_title();
?>
<?php while ( have_posts() ) : the_post(); ?>
<div class="<?php echo esc_attr( $container ); ?> tribe-events-venue">
	<div class="tribe-events-venue-meta">
		<!-- Venue Meta -->
		<?php do_action( 'tribe_events_single_venue_before_the_meta' ) ?>

		<div class="venue-address border border-light p-3 mb-3">

			<?php if ( $full_address ) : ?>
			<address class="tribe-events-address">
				<h2>Address</h2>
				<?php echo $full_address; ?>
			</address>
			<?php endif; ?>

			<?php if ( $telephone ): ?>
				<p class="tel">
					<?php echo $telephone; ?>
				</p>
			<?php endif; ?>
		</div><!-- .venue-address -->

		<?php do_action( 'tribe_events_single_venue_after_the_meta' ) ?>

	</div><!-- .tribe-events-event-meta -->

	<!-- Venue Description -->
	<?php if ( get_the_content() ) : ?>
	<div class="tribe-venue-description tribe-events-content">
		<?php the_content(); ?>
	</div>
	<?php endif; ?>

	<!-- Venue Featured Image -->
	<?php echo tribe_event_featured_image( null, 'full' ) ?>

	<!-- Upcoming event list -->
	<?php do_action( 'tribe_events_single_venue_before_upcoming_events' ) ?>

	<?php
	// Use the `tribe_events_single_venue_posts_per_page` to filter the number of events to get here.
	// Close the container DIV before the loop, which has its own container.
	echo str_replace( '<!-- Events Loop -->', '<h2 class="bg-light">Upcoming Events at this Location</h2></div><!-- Events Loop -->', tribe_venue_upcoming_events( $venue_id, $wp_query->query_vars ) ); ?>

	<?php do_action( 'tribe_events_single_venue_after_upcoming_events' ) ?>


<?php
endwhile;
