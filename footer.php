<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( empty( $GLOBALS[ 'page_settings' ][ 'hide_footer' ] ) || $GLOBALS[ 'page_settings' ][ 'hide_footer' ] != 'on' ) : ?>
	<footer id="wrapper-footer">
		<?php
			// Does this page use a custom footer?
			if ( !empty( $GLOBALS[ 'page_settings' ][ 'custom_footer_id' ] ) && is_numeric( $GLOBALS[ 'page_settings' ][ 'custom_footer_id' ] ) ) {
				echo gradpress_maybe_wrap_content_in_row( gradpress_get_page_include( $GLOBALS[ 'page_settings' ][ 'custom_footer_id' ] ) );
			}
			else if ( !empty( $GLOBALS[ 'post' ]->ID ) ) {
				// Does one of this page's ancestors use a custom footer?
				$ancestors = get_ancestors( $GLOBALS[ 'post' ]->ID, 'page' );
				$custom_footer_id = 0;

				foreach( $ancestors as $a_ancestor_id ) {
					if ( empty( $a_ancestor_id ) || get_the_title( $a_ancestor_id ) == '' ) {
						continue;
					}
		
					$gradpress_page_settings = get_post_meta( $a_ancestor_id, '_gradpress_page_settings', true );
					if ( !empty( $gradpress_page_settings[ 'custom_footer_id' ] ) ) {
						$custom_footer_id = $gradpress_page_settings[ 'custom_footer_id' ];
						break;
					}
				}

				if ( !empty( $custom_footer_id ) ) {
					echo gradpress_maybe_wrap_content_in_row( gradpress_get_page_include( $custom_footer_id ) );
				}
				else if ( is_active_sidebar( 'footerfull' ) ) {
					echo gradpress_maybe_wrap_content_in_row( gradpress_get_dynamic_sidebar( 'footerfull' ) );
				}	
			}
		?>
	</footer>
<?php endif;

wp_footer();

?>
</body>
</html>