// Requirements (alphabetical order)
var gulp = require( 'gulp' );

var autoprefixer = require( 'autoprefixer' );
var clean_css = require( 'gulp-clean-css' );
var concat = require( 'gulp-concat' );
var del = require( 'del' );
var postcss = require( 'gulp-postcss' );
var plumber = require( 'gulp-plumber' );
var rename = require( 'gulp-rename' );
var sass = require( 'gulp-sass' );
var sourcemaps = require( 'gulp-sourcemaps' );
var uglify = require( 'gulp-uglify' );
var watch = require( 'gulp-watch' );

// Folder structure
var paths = {
	"js": "./js",
	"css": "./css",
	"src": "./src",
	"node": "./node_modules"
};

// Run:
// gulp copy-assets
// This task is called after you run 'npm update' to resync all the packages
gulp.task( 'copy-assets', function( done ) {
	// Update Bootstrap SCSS files
	del( paths.src + '/sass/vendor/bootstrap/**/*' );
	gulp.src( paths.node + '/bootstrap/scss/**/*' )
		.pipe( gulp.dest( paths.src + '/sass/vendor/bootstrap' ) );

	// Copy Bootstrap JS files
	del( paths.src + '/js/vendor/bootstrap/**/*' );
	gulp.src( paths.node + '/bootstrap/dist/js/**/bootstrap.bundle.*.js' )
		.pipe( gulp.dest( paths.src + '/js/vendor/bootstrap' ) );

	// Copy Font Awesome SCSS files
	del( paths.src + '/sass/vendor/font-awesome/**/*' );
	gulp.src( paths.node + '/font-awesome/scss/*.scss' )
		.pipe( gulp.dest( paths.src + '/sass/vendor/font-awesome' ) );

	// Copy Font Awesome Fonts
	del( paths.css + '/fonts/**/*' );
	gulp.src( paths.node + '/font-awesome/fonts/**/*.{ttf,woff,woff2,eot,svg}' )
		.pipe( gulp.dest( paths.css + '/fonts' ) );
		
	// Copy Jarallax JS files: core file is renamed so that it's processed first when combined into theme.min.js
	del( paths.src + '/js/vendor/jarallax/**/*' );
	gulp.src( paths.node + '/jarallax/dist/jarallax.min.js' )
		.pipe( rename( 'jarallax-core.min.js' ) )
		.pipe( gulp.dest( paths.src + '/js/vendor/jarallax' ) );

	gulp.src( paths.node + '/jarallax/dist/jarallax-video.min.js' )
		.pipe( gulp.dest( paths.src + '/js/vendor/jarallax' ) );

	done();
} );

// Run:
// gulp minifycss
// Minifies CSS files
gulp.task( 'minifycss', function() {
	return gulp.src( [ paths.css + '/theme.css', paths.css + '/admin.css', paths.css + '/frontend_editor.css' ] )
		.pipe( sourcemaps.init( { loadMaps: true } ) )
		.pipe( clean_css( { compatibility: '*' } ) )
		.pipe( plumber( {
				errorHandler: function( err ) {
					console.log( err ) ;
					this.emit( 'end' );
				}
			} ) )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( sourcemaps.write( './' ) )
		.pipe( gulp.dest( paths.css ) );
} );

// Run:
// gulp sass
// Compiles SCSS files in CSS
gulp.task( 'sass', function() {
	return gulp
		.src( [ paths.src + '/sass/theme.scss', paths.src + '/sass/admin/admin.scss', paths.src + '/sass/admin/frontend_editor.scss' ] )
		.pipe( plumber( {
			errorHandler: function( err ) { 
				console.log( err );
				this.emit( 'end' );
			}
		} ) )
		.pipe( sourcemaps.init( { loadMaps: true } ) )
		.pipe( sass( { errLogToConsole: true } ) )
		.pipe( postcss( [ autoprefixer() ] ) )
		.pipe( sourcemaps.write( undefined, { sourceRoot: null } ) )
		.pipe( gulp.dest( paths.css ) );
} );

// Run:
// gulp scripts
// Uglifies and concat all JS files into one
gulp.task( 'scripts', function() {
	gulp.src( [ paths.src + '/js/vendor/**/*.min.js', paths.src + '/js/theme/**/*.js' ] )
		.pipe( concat( 'theme.min.js' ) )
		.pipe( uglify() )
		.pipe( gulp.dest( paths.js ) );
	
	return gulp
		.src( [ paths.src + '/js/admin/admin.js' ] )
		.pipe( uglify() )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( paths.js ) );
} );

// Run:
// gulp styles
// Compiles SCSS files in CSS, and minify the output
gulp.task( 'styles', function( callback ) {
	gulp.series( 'sass', 'minifycss' )( callback );
} );

// Run:
// gulp watch
// Starts watcher. Watcher runs gulp sass task on changes
gulp.task( 'watch', function() {
	gulp.watch( paths.src + '/sass/**/*.scss', { interval: 2000, usePolling: true }, gulp.series( 'styles' ) );
	gulp.watch( paths.src + '/js/**/*.js', { interval: 2000, usePolling: true }, gulp.series( 'scripts' ) );
} );