<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Retrieve the background color for the page title from the global settings (Customizer)
$page_title_bg_color = get_theme_mod( 'understrap_page_title_bg_color', 'bg-light' );

get_header();

if ( have_posts() ) : the_post();

$content_has_row = gradpress_content_has_row( apply_filters( 'the_content', get_the_content() ) );

// Container Type
if ( !$content_has_row ) {
	$container = get_theme_mod( 'understrap_container_type', 'container' );
}

?>

<main id="content">
	<?php gradpress_the_title(); ?>

<?php if ( !$content_has_row ): ?>
	<div class="<?php echo esc_attr( $container ); ?>" tabindex="-1">
		<div class="row">
			<?php get_template_part( 'sidebar-templates/sidebar-left' ); ?>
			<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
				<?php understrap_entry_footer(); ?>
				<?php the_content(); ?>
			</article>

			<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
				'after'  => '</div>',
			) );
			
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

			get_template_part( 'sidebar-templates/sidebar-right' ); ?>
		</div><!-- .row -->
	</div><!-- .container -->
<?php else: ?>
	<?php the_content(); ?>
<?php endif; ?>
</main>

<?php endif; get_footer();