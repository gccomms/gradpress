<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

if ( have_posts() ) : the_post();
?>
<main id="content">
	<?php
	if ( !is_front_page() && ( empty( $GLOBALS[ 'page_settings' ][ 'hide_page_title' ] ) || $GLOBALS[ 'page_settings' ][ 'hide_page_title' ] != 'on' ) ) {
		gradpress_the_title();
	}
	?>
	<article <?php post_class(); ?> id="page-<?php the_ID(); ?>">
	<?php 
		$content = get_the_content();
		$content = apply_filters( 'the_content', $content );
		$content = str_replace( ']]>', ']]&gt;', $content );

		if ( !gradpress_content_has_row( $content ) ) {
			echo gradpress_maybe_wrap_content_in_row( $content );
		}
		else {
			echo $content;
		}
	?>
	</article>
</main>

<?php endif; get_footer();
