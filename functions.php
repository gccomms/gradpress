<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/setup.php',                           // Theme setup and custom theme supports.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/page-title.php',                    	// Defines a function to display page title and breadcrumbs.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/search.php',    						// Hooks and functions to implement the advanced search modal.
	'/editor.php',                          // Load Editor functions.
	'/page-settings.php',                   // Defines page metaboxes to customize the page behavior on an individual basis.
	'/wp-bakery-pagebuilder.php',			// Compatibility with WP Bakery Page Builder.
	'/the-events-calendar.php'				// Compatibility with WP Bakery Page Builder.
);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}