<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Retrieve the background color for the page title from the global settings (Customizer)
$page_title_bg_color = get_theme_mod( 'understrap_page_title_bg_color', 'bg-light' );
$search_url = get_theme_mod( 'understrap_search_url', '' );
$earch_input_name = get_theme_mod( 'understrap_search_input_name', 's' );
$search_custom_input_fields = get_theme_mod( 'understrap_search_custom_input_fields', '' );
$container = get_theme_mod( 'understrap_container_type', '' );

get_header();

?>

<main id="content">
	<?php gradpress_the_title(); ?>

	<div class="<?php echo esc_attr( $container ); ?>" tabindex="-1">
		<div class="row">
			<?php get_template_part( 'sidebar-templates/sidebar-left' ); ?>
			<?php if ( have_posts() ) : ?>
			<article <?php post_class( 'wpb_text_column wpb_content_element' ); ?> id="post-<?php the_ID(); ?>">
				<?php the_content(); ?>
				<?php understrap_entry_footer(); ?>
			</article>
			<?php else: ?>
			<article>
				Sorry, the page that you requested doesn't exist. Please use the form below to search our website.
				<form class="input-group py-4" action="<?php echo $search_url ?>">
					<input class="form-control py-4 border-right-0 border" type="search" placeholder="Search" id="example-search-input" name="<?php echo $earch_input_name ?>">
					<span class="input-group-append">
						<button class="btn border-left-0 border" type="submit">
							<i class="fa fa-search"></i>
						</button>
					</span>
					<?php echo $search_custom_input_fields ?>
				</form>
			</article>
			<?php endif; ?>

			<?php get_template_part( 'sidebar-templates/sidebar-right' ); ?>
		</div><!-- .row -->
	</div>
</main>

<?php get_footer(); ?>