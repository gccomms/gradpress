<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$page_title_bg_color = get_theme_mod( 'understrap_page_title_bg_color', 'bg-light' );
$container = get_theme_mod( 'understrap_container_type', 'container' );

get_header();

?>
<main id="content">
	<?php gradpress_the_title(); ?>

	<div class="<?php echo esc_attr( $container ); ?>">
		<div class="row">
			<?php get_template_part( 'sidebar-templates/sidebar-left' ); ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<article <?php post_class( 'wpb_text_column wpb_content_element' ); ?> id="post-<?php the_ID(); ?>">
				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
				<?php understrap_entry_footer(); ?>
				<?php the_excerpt(); ?>
			</article>
			<?php endwhile; ?>
			<?php understrap_pagination(); ?>
			<?php get_template_part( 'sidebar-templates/sidebar-right' ); ?>
		</div><!-- .row -->
	</div><!-- .container -->
</main>

<?php get_footer(); ?>
