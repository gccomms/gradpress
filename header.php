<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Is this page being redirected to a VALID relative/absolute URL?
$redirect_url = '';
if ( !empty( $GLOBALS[ 'page_settings' ][ 'redirect_url' ] ) && ( substr( $GLOBALS[ 'page_settings' ][ 'redirect_url' ], 0, 1 ) == '/' || filter_var( trim( $GLOBALS[ 'page_settings' ][ 'redirect_url' ] ), FILTER_VALIDATE_URL ) !== false ) ) {
	$redirect_url = $GLOBALS[ 'page_settings' ][ 'redirect_url' ];
}

if ( !empty( $redirect_url ) ) {
	header( 'HTTP/1.1 301 Moved Permanently' );
	header( 'Location: ' . $GLOBALS[ 'page_settings' ][ 'redirect_url' ] );
	exit;
}

$header_logo = get_theme_mod( 'understrap_site_logo', '' );
$header_style = get_theme_mod( 'understrap_header_style', 'multirow' );
$header_sticky = get_theme_mod( 'understrap_header_stickiness', 'normal' );
$header_bg_color = get_theme_mod( 'understrap_header_bg_color', 'bg-light' );
$header_logo_alignment = get_theme_mod( 'understrap_header_logo_alignment', 'center' );
$header_modal_button_color = get_theme_mod( 'understrap_header_modal_button_color', 'navbar-dark' );
$header_nav_placement = get_theme_mod( 'understrap_header_nav_placement', 'above' );
$header_nav_breakpoint = get_theme_mod( 'understrap_header_nav_breakpoint', 'xl' );
$header_modal_icon = get_theme_mod( 'understrap_header_modal_icon', 'fa-search' );
$nav_bg_color = get_theme_mod( 'understrap_primary_nav_bg_color', 'bg-light' );
$nav_color_scheme = get_theme_mod( 'understrap_primary_nav_color_scheme', 'navbar-light' );
$additional_head_code = get_theme_mod( 'understrap_additional_head_code', '' );
$container_type = get_theme_mod( 'understrap_container_type', 'container' );

// Does this page use a custom header?
$custom_header_id = 0;
if ( !empty( $GLOBALS[ 'page_settings' ][ 'custom_header_id' ] ) && is_numeric( $GLOBALS[ 'page_settings' ][ 'custom_header_id' ] ) ) {
	$custom_header_id = $GLOBALS[ 'page_settings' ][ 'custom_header_id' ];
}
else if ( !empty( $GLOBALS[ 'post' ]->ID ) ) {
	// Does one of this page's ancestors use a custom header?
	$ancestors = get_ancestors( $GLOBALS[ 'post' ]->ID, 'page' );

	foreach( $ancestors as $a_ancestor_id ) {
		if ( empty( $a_ancestor_id ) || get_the_title( $a_ancestor_id ) == '' ) {
			continue;
		}

		$gradpress_page_settings = get_post_meta( $a_ancestor_id, '_gradpress_page_settings', true );
		if ( !empty( $gradpress_page_settings[ 'custom_header_id' ] ) ) {
			$custom_header_id = $gradpress_page_settings[ 'custom_header_id' ];
			break;
		}
	}
}
if ( !empty( $custom_header_id ) ) {
	$header_style = 'multirow';
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<?php echo $additional_head_code; ?>
</head>
<?php 
// No need to do anything else, if this page is being redirected and the HTTP header directive didn't work
if ( !empty( $GLOBALS[ 'page_settings' ][ 'redirect_url' ] ) && filter_var( $GLOBALS[ 'page_settings' ][ 'redirect_url' ], FILTER_VALIDATE_URL) !== false ) {
	echo '<body></body></html>';
	exit;
}
?>
<body <?php body_class(); ?>>

<a class="skip-link sr-only sr-only-focusable" href="#content">Skip to content</a>

<?php if ( empty( $GLOBALS[ 'page_settings' ][ 'hide_header' ] ) || $GLOBALS[ 'page_settings' ][ 'hide_header' ] != 'on' ): ?>
<header class="<?php echo esc_attr( $header_sticky . ' ' . $header_bg_color . ' ' . $header_style ); ?> shadow-sm" id="wrapper-header">
<?php if ( $header_style == 'inline' ): ?>
	<div class="<?php echo esc_attr( $container_type ) ?>">
		<section class="row row-o-equal-height row-o-content-middle row-flex">
			<div class="col-6 col-<?php echo $header_nav_breakpoint ?>-3 <?php echo $nav_color_scheme ?>">
				<div class="vc_column-inner navbar-nav">
					<a href="<?php echo get_home_url() ?>" alt="" class="nav-link">
					<?php if ( !empty( $header_logo ) ) {
						echo '<img src="' . $header_logo . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '">';
					}
					else {
						echo esc_attr( get_bloginfo( 'name' ) );
					}
					?>
					</a>
				</div>
			</div>

			<div class="col-6 col-<?php echo $header_nav_breakpoint ?>-9">
				<div class="vc_column-inner">
					<div class="navbar-expand-<?php echo $header_nav_breakpoint ?> float-right">
						<div class="navbar-nav d-none d-<?php echo $header_nav_breakpoint ?>-block <?php echo $header_modal_button_color ?> float-right">
							<?php echo understrap_get_modal_button( $header_modal_icon ); ?>
						</div>
						<div class="navbar-nav d-<?php echo $header_nav_breakpoint ?>-none <?php echo $header_modal_button_color ?>">
							<?php echo understrap_get_modal_button( 'fa-bars' ); ?>
						</div>
					</div>
					<?php if ( has_nav_menu( 'primary' ) ): ?>
					<nav role="navigation" class="navbar navbar-expand-<?php echo $header_nav_breakpoint ?> float-right px-0 <?php echo $nav_color_scheme ?>" id="navbar-primary">
					<?php
						wp_nav_menu( array(
							'theme_location'    => 'primary',
							'depth'             => 2,
							'container'         => 'div',
							'container_class'   => 'collapse navbar-collapse',
							'container_id'      => 'bs-navbar-collapse-primary',
							'menu_class'        => 'nav navbar-nav',
							'li_class' 			=> 'your-class-name1 your-class-name-2',
							'fallback_cb'       => 'Understrap_WP_Bootstrap_Navwalker::fallback',
							'walker'            => new Understrap_WP_Bootstrap_Navwalker()
						) );
					?>
					</nav>
					<?php endif; // has_nav_menu( 'primary' ) ?>
				</div><!-- .vc_column-inner -->
			</div><!-- .col-6 -->
		</section><!-- .row -->
	</div><!-- $container_type -->
<?php elseif ( $header_style == 'multirow' && ( empty( $GLOBALS[ 'page_settings' ][ 'hide_header' ] ) || $GLOBALS[ 'page_settings' ][ 'hide_header' ] != 'on' ) ): ?>
	<?php if ( empty( $header_nav_placement ) || $header_nav_placement == 'above' ): ?>
	<div class="d-none d-<?php echo esc_attr( $header_nav_breakpoint ) . '-block ' . esc_attr( $nav_bg_color ) . ' ' . esc_attr( $nav_color_scheme ); ?>">
		<div class="<?php echo esc_attr( $container_type ) ?>">
			<section class="row row-o-equal-height row-o-content-middle row-flex">
				<div class="col-11">
					<div class="vc_column-inner">
						<?php if ( has_nav_menu( 'primary' ) ): ?>
						<nav role="navigation" class="navbar navbar-expand-<?php echo $header_nav_breakpoint ?> px-0 <?php echo $nav_color_scheme ?>" id="navbar-primary">
						<?php
							wp_nav_menu( array(
								'theme_location'    => 'primary',
								'depth'             => 2,
								'container'         => 'div',
								'container_class'   => 'collapse navbar-collapse',
								'container_id'      => 'bs-navbar-collapse-primary',
								'menu_class'        => 'nav navbar-nav',
								'li_class' 			=> 'your-class-name1 your-class-name-2',
								'fallback_cb'       => 'Understrap_WP_Bootstrap_Navwalker::fallback',
								'walker'            => new Understrap_WP_Bootstrap_Navwalker()
							) );
						?>
						</nav>
						<?php endif; // has_nav_menu( 'primary' ) ?>
					</div><!-- .vc_column-inner -->
				</div><!-- .col-11 -->
				<div class="col-1">
					<div class="vc_column-inner">
						<div class="nav navbar-nav float-right <?php echo $header_modal_button_color ?>">
							<?php echo understrap_get_modal_button( $header_modal_icon ); ?>
						</div>
					</div><!-- .vc_column-inner -->
				</div><!-- .col-1 -->
			</section><!-- .row -->
		</div><!-- $container_type -->
	</div><!-- .d-none -->
	<?php endif; ?>

	<?php
		// Does this page use a custom header?
		if ( !empty( $custom_header_id ) ) {
			echo gradpress_maybe_wrap_content_in_row( gradpress_get_page_include( $custom_header_id ) );
		}
		else { 
	?>
	<div class="<?php echo esc_attr( $container_type ) ?>">
		<section class="row row-o-content-middle row-flex">
			<div class="col-10 col-<?php echo $header_nav_breakpoint ?>-12">
				<div class="vc_column-inner py-3 d-flex justify-content-start justify-content-<?php echo $header_nav_breakpoint ?>-<?php echo $header_logo_alignment ?>">
					<a href="<?php echo get_home_url() ?>">
						<?php if ( !empty( $header_logo ) ) {
							echo '<img src="' . $header_logo . '" alt="' . esc_attr( get_bloginfo( 'name' ) ) . '">';
						}
						else {
							echo esc_attr( get_bloginfo( 'name' ) );
						}
						?>
					</a>
				</div><!-- .vc_column-inner -->
			</div><!-- .col-11 -->
			<div class="col-2 d-<?php echo $header_nav_breakpoint ?>-none">
				<div class="vc_column-inner">
					<div class="nav navbar-nav justify-content-end <?php echo $header_modal_button_color ?>">
						<?php echo understrap_get_modal_button( 'fa-bars' ); ?>
					</div>
				</div><!-- .vc_column-inner -->
			</div><!-- .col-12 -->
		</section><!-- .row -->
	</div><!-- $container_type -->

	<?php } // else empty( $custom_header_id )

	if ( $header_nav_placement == 'under' ): ?>
	<div class="d-none d-<?php echo esc_attr( $header_nav_breakpoint ) . '-block ' . esc_attr( $nav_bg_color ) . ' ' . esc_attr( $nav_color_scheme ); ?>">
		<div class="<?php echo esc_attr( $container_type ) ?>">
			<section class="row row-o-equal-height row-o-content-middle row-flex">
				<div class="col-11">
					<div class="vc_column-inner">
						<?php if ( has_nav_menu( 'primary' ) ): ?>
						<nav role="navigation" class="navbar navbar-expand-<?php echo $header_nav_breakpoint ?> px-0 <?php echo $nav_color_scheme ?>" id="navbar-primary">
						<?php
							wp_nav_menu( array(
								'theme_location'    => 'primary',
								'depth'             => 2,
								'container'         => 'div',
								'container_class'   => 'collapse navbar-collapse',
								'container_id'      => 'bs-navbar-collapse-primary',
								'menu_class'        => 'nav navbar-nav',
								'li_class' 			=> 'your-class-name1 your-class-name-2',
								'fallback_cb'       => 'Understrap_WP_Bootstrap_Navwalker::fallback',
								'walker'            => new Understrap_WP_Bootstrap_Navwalker()
							) );
						?>
						</nav>
						<?php endif; // has_nav_menu( 'primary' ) ?>
					</div><!-- .vc_column-inner -->
				</div><!-- .col-11 -->
				<div class="col-1">
					<div class="vc_column-inner">
						<div class="nav navbar-nav float-right <?php echo $header_modal_button_color ?>">
							<?php echo understrap_get_modal_button( $header_modal_icon ); ?>
						</div>
					</div><!-- .vc_column-inner -->
				</div><!-- .col-1 -->
			</section><!-- .row -->
		</div><!-- $container_type -->
	</div><!-- .d-none -->
	<?php endif;

	if ( !empty( $custom_header_id ) ): ?>
	<div class="d-<?php echo esc_attr( $header_nav_breakpoint ) . '-none ' . esc_attr( $nav_bg_color ) . ' ' . esc_attr( $nav_color_scheme ); ?>">
		<div class="<?php echo esc_attr( $container_type ) ?>">
			<section class="row row-flex">
				<div class="col-12">
					<div class="vc_column-inner">
						<div class="nav navbar-nav justify-content-center <?php echo $header_modal_button_color ?>">
							<?php echo understrap_get_modal_button( 'fa-bars' ); ?>
						</div>
					</div><!-- .vc_column-inner -->
				</div><!-- .col-12 -->
			</section><!-- .row -->
		</div><!-- $container_type -->
	</div><!-- .d-xx-none -->
	<?php endif; ?>

<?php endif; // else $header_style == 'multirow' ?>
</div>
</header>
<?php endif; // if hide_header is not selected ?>